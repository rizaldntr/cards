# Wiki

## Project structure
First, we talk about project structure.
- [/apib](../apib) this folder to store our API Blueprint.
- [/cmd](../cmd) this folder to store our main go, currently we only have rest sub-folder. Let's say we want to expose our data using Grpc we can create new folder for that.
- [/db](.../db) this folder to store any data related to db, for example our migrations file.
- [/docs](./) this folder, as you see right now to store any documentations about this project.
- [/internal](../internal) this is to store our private code, it's not intended to be used by other projects.
    - [/application](../internal/application) to store data to help us for dependency injection
    - [/config](../internal/config) to generate config, usually we read from env that convert it to struct
    - [/constant](../internal/constant) to store any constant that will be used in this project.
    - [/delivery](../internal/delivery) to store implementation about our delivery, currently, we only have simple rest API. Let's say we want to use GRPC or any event subscriber we can define it on this folder. 
    - [/errors](../internal/errors) to store any defined errors
    - [/model](../internal/errors) to store our db model and interfaces
    - [/repository](../internal/repository) to store our implementation for connection to database. First you must define the interface in [/model](../internal/model).
    - [/usecase](../internal/usecase) to store our usecase implementation, it's like service implementation.
- [/locust](../locust) to store our loadtest file using locust
- [/mocks](../mocks) generated mock using mockery
- [/pkg](../pkg) to store our implementation that other team can use it, for example middleware on this project.

In short, handler/delivery will use usecase when serving the request and the usecase will use repository to connect to databases.

## Database
Let's talk about our application, it's about financial service, consistency is a must. What will user saying if they actually have 200$, but sometimes they got 200$ and sometimes other than that?
User will worrying about their money right? The second one is about availability, it's very often when something important happens we need that money, what happen if our db down and user can make a transactions? I guess user will try to use another platform to store their money.

In short, our DB needs:
1. Consistency
2. Availability

For this kind of requirement it's better for use to use SQL database like PostgreSQL.