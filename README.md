# Cards

## Description
Very simple API implementations for managing user's data, user's wallets and user's card. 

## Technology
- [Golang - Build fast, reliable, and efficient software at scale](https://go.dev/)
- [httprouter - a lightweight high performance HTTP request router](https://github.com/julienschmidt/httprouter)
- [PostgreSQL - The World's Most Advanced Open Source Relational Database](https://www.postgresql.org/)
- [Gorm - The fantastic ORM library for Golang](https://gorm.io/index.html)
- [Prometheus - From metrics to insight](https://prometheus.io/)
- [Opentracing - Vendor-neutral APIs and instrumentation for distributed tracing](https://opentracing.io/)
- [Jaeger - Jaeger: open source, end-to-end distributed tracing](https://www.jaegertracing.io/)

## Installation

### Prerequisite
1. [Golang 1.16](https://go.dev/doc/install)
2. [PostgreSQL](https://www.postgresql.org/download/)
3. [Jaeger](https://www.jaegertracing.io/download/)

### Build
```
go mod download
go build -o main ./cmd/rest
```

### Run
Populate env data

```
cp env.sample .env
```

Then

`./main`

If you want magic, I already created it for you (but you still have to install docker and docker-compose hehe)

```
make magic
```
Then this is for you.
1. http://localhost:8000/ this is our service.
2. http://localhost:8088/ for API Blueprint.
3. http://localhost:9090/ for prometheus.
4. http://localhost:16686/ for jaeger tracing.

## Roadmap
1. Currently, card limit is on card table and my idea to reset the data is by using cron job on every day and every month to reset daily_usage and monthly usage.
But I think it is not good because if the cron is failed then the data is not accurate. To monitor the cron itself its not easy task. My next proposal is to create another
table called `card_transactions` which have from_wallet_id, to_wallet_id, amount then card table will only use to storing the limit. Every transaction will lookup to that table to count the daily limit and monthly limit.
If there is performance issue, we can try to use expression index, but i'm still not sure if this worthy, need to do load test for that.
2. Improve code quality, some functions still not well documented and tested. I will put linter and test on pipeline before any code merge to master.
3. To have better observability, I think we can span more context on the very long process.
4. Implement Publisher on data changes, its very often that another service need to know if user's data changed. Instead of making pooling request to check if the data changed, they can just subscribe to our publisher.

## Database Schema
![Database Schema](./db/diagram.png "DB Schema Diagram")

## Project status
Still in development.

## FAQ
1. If you have problem with CORS when accessing API Blueprint,you can use this extension in google-chrome.
2. Any other details you can see on [/docs](docs)