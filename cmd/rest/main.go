package main

import (
	"gitlab.com/rizaldntr/cards/internal/application"
	"gitlab.com/rizaldntr/cards/internal/delivery/rest"
	"gitlab.com/rizaldntr/cards/pkg/exithandler"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gitlab.com/rizaldntr/cards/pkg/server"
)

func main() {
	app, err := application.Get()
	if err != nil {
		log.Error.Fatal(err.Error())
	}

	defer app.Tracing.Closer.Close()

	srv := server.Get().
		WithAddr(app.Cfg.GetAPIPort()).
		WithRouter(rest.Get(app)).
		WithErrLogger(log.Error)

	go func() {
		log.Info.Printf("starting server at %s", app.Cfg.GetAPIPort())
		if err := srv.Start(); err != nil {
			log.Error.Fatal(err.Error())
		}
	}()

	exithandler.Init(func() {
		if err := srv.Close(); err != nil {
			log.Error.Println(err.Error())
		}

		if err := app.DB.Close(); err != nil {
			log.Error.Println(err.Error())
		}
	})
}
