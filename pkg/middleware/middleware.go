package middleware

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"github.com/opentracing/opentracing-go"
	pkgErr "github.com/pkg/errors"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gitlab.com/rizaldntr/cards/pkg/response"
	"golang.org/x/time/rate"
)

// HandleWithError is a httprouter.Handle that returns an error.
type HandleWithError func(http.ResponseWriter, *http.Request, httprouter.Params) error

// Middleware decorates HandleWithError.
type Middleware func(HandleWithError) HandleWithError

// UseMiddleware runs HandleWithError and converts it to httprouter.Handle.
// The conversion is needed because httprouter.Router needs httprouter.Handle
// in its signature.
func UseMiddleware(handle HandleWithError, middlewares ...Middleware) httprouter.Handle {
	for _, mw := range middlewares {
		handle = mw(handle)
	}

	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		handle(w, r, params)
	}
}

// WithTimeout decorates Decorator with timeout.
func WithTimeout(timeout time.Duration) Middleware {
	return func(handle HandleWithError) HandleWithError {
		return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) error {
			ctx := r.Context()
			ctx, cancel := context.WithTimeout(ctx, timeout)
			defer cancel()

			r = r.WithContext(ctx)

			errCh := make(chan error, 1)
			panicCh := make(chan interface{}, 1)

			go func() {
				defer func() {
					if p := recover(); p != nil {
						panicCh <- p
					}
				}()

				errCh <- handle(w, r, params)
				close(errCh)
			}()

			select {
			case p := <-panicCh:
				panic(p)
			case <-ctx.Done():
				w.WriteHeader(http.StatusServiceUnavailable)

				return errors.New("request timeout. probably the service is unavailable")
			case err := <-errCh:
				return err
			}
		}
	}
}

// WithRateLimit decorates decorator with ratelimit.
// the decorator will rate limit based on authorization.
func WithRateLimit(limit rate.Limit, burst int) Middleware {
	serviceLimiterMap := make(map[string]*rate.Limiter)
	m := &sync.Mutex{}

	return func(handle HandleWithError) HandleWithError {
		return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) error {
			serviceID := r.Header.Get("Authorization")
			if serviceID == "" {
				serviceID = "default"
			}

			m.Lock()
			lim, ok := serviceLimiterMap[serviceID]
			if !ok {
				lim = rate.NewLimiter(limit, burst)
				serviceLimiterMap[serviceID] = lim
			}
			m.Unlock()

			if !lim.Allow() {
				w.WriteHeader(http.StatusTooManyRequests)

				return errors.New("too many requests")
			}

			return handle(w, r, params)
		}
	}
}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func newLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

// HTTPObserver interface to observer api latency with http status
type HTTPObserver interface {
	Observe(ctx context.Context, duration float64, method, path, status string, httpStatus int)
}

// WithHTTPStatusInstrumentation decorates HandleWithError with instrumentation.
func WithHTTPStatusInstrumentation(observer HTTPObserver) Middleware {
	return func(handle HandleWithError) HandleWithError {
		return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) error {
			start := time.Now()
			status := "ok"

			lwr := newLoggingResponseWriter(w)
			err := handle(w, r, params)
			if err != nil {
				status = "fail"
			}

			observer.Observe(r.Context(), time.Since(start).Seconds(), r.Method, trimLeak(r.URL.Path, params), status, lwr.statusCode)

			return err
		}
	}
}

type ctxKey string

// WithStandardContext decorates Decorator with standard context.
func WithStandardContext() Middleware {
	return func(handle HandleWithError) HandleWithError {
		return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) error {
			ctx := r.Context()

			if r.Header.Get("X-Request-ID") == "" {
				reqID, err := createRequestID()
				if err != nil {
					return err
				}
				r.Header.Set("X-Request-ID", reqID)
			}

			ctx = context.WithValue(ctx, ctxKey("request_id"), r.Header.Get("X-Request-ID"))

			return handle(w, r.WithContext(ctx), params)
		}
	}
}

type tracer interface {
	StackTrace() pkgErr.StackTrace
}

type ContextResolver interface {
	Metadata(context.Context) map[string]interface{}
}

type NoopContextResolver struct{}

func (n *NoopContextResolver) Metadata(_ context.Context) map[string]interface{} {
	return nil
}

// WithLogging decorates Decorator with logging.
func WithLogging(logger *log.Logger, cr ContextResolver) Middleware {
	return func(handle HandleWithError) HandleWithError {
		return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) error {
			reqID := r.Header.Get("X-Request-ID")
			start := time.Now()

			lwr := newLoggingResponseWriter(w)
			err := handle(lwr, r, params)

			// elapsed time in milliseconds
			elapsed := time.Since(start).Seconds() * 1000
			elapsedStr := strconv.FormatFloat(elapsed, 'f', -1, 64)

			var logFields []log.Field
			metadata := cr.Metadata(r.Context())
			for k, v := range metadata {
				logFields = append(logFields, log.NewField(k, v))
			}

			logFields = append(logFields,
				log.NewField("request_id", reqID),
				log.NewField("duration", elapsedStr),
				log.NewField("full_url", r.URL.String()),
				log.NewField("tags", []string{r.URL.Path, r.Method}),
				log.NewField("http_status", lwr.statusCode),
			)

			if err != nil {
				logFields = append(logFields,
					log.NewField("stacktrace", stacktrace(err)),
				)
				logger.Error(err.Error(), logFields...)
			} else {
				logger.Info("OK", logFields...)
			}

			return err
		}
	}
}

// WithTracing decorates Decorator with tracing.
// The tracer itself must be an opentracing.Tracer.
func WithTracing(tracer opentracing.Tracer) Middleware {
	return func(handle HandleWithError) HandleWithError {
		return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) error {
			var span opentracing.Span

			wireContext, err := tracer.Extract(
				opentracing.TextMap,
				opentracing.HTTPHeadersCarrier(r.Header),
			)

			if err != nil {
				span = opentracing.StartSpan(r.URL.Path)
			} else {
				span = opentracing.StartSpan(r.URL.Path, opentracing.ChildOf(wireContext))
			}

			defer span.Finish()
			ctx := opentracing.ContextWithSpan(r.Context(), span)

			return handle(w, r.WithContext(ctx), params)
		}
	}
}

// trimLeak transform parameter value to its own key
func trimLeak(path string, params httprouter.Params) string {
	for idx, p := range params {
		// no match then continue
		if p.Value == "" {
			continue
		}

		// its wildcard if contains "/"
		if strings.Contains(p.Value, "/") && idx+1 == len(params) {
			return strings.TrimSuffix(path, p.Value) + "/*" + p.Key
		}

		// else normal named parameter
		pOld := "/" + p.Value
		pNew := "/:" + p.Key
		if idx+1 != len(params) {
			pOld = pOld + "/"
			pNew = pNew + "/"
		}
		path = strings.Replace(path, pOld, pNew, 1)
	}

	return path
}

func createRequestID() (string, error) {
	temp, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}

	return temp.String(), nil
}

func stacktrace(errLog error) string {
	var newError error
	var isCustomError bool
	var traces string

	if _, ok := errLog.(response.CustomError); ok {
		newError = errLog.(response.CustomError).Err
		isCustomError = true
	} else {
		newError = errLog
		isCustomError = false
	}

	err, ok := newError.(tracer)
	if !ok {
		return ""
	}

	for idx, f := range err.StackTrace() {
		if isCustomError && idx == 0 {
			continue
		}

		traces += fmt.Sprintf("%+v\n", f)
	}

	return traces
}
