package middleware_test

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/opentracing/opentracing-go"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gitlab.com/rizaldntr/cards/pkg/middleware"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func TestWithTimeout(t *testing.T) {
	d := middleware.WithTimeout(300 * time.Millisecond)
	router := httprouter.New()

	// success
	fs := d(success)
	router.GET("/success", middleware.UseMiddleware(fs))
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/success", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)

	// fail
	ff := d(fail)
	router.GET("/fail", middleware.UseMiddleware(ff))
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/fail", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)

	// check for returned nil
	fn := d(returnNil)
	err := fn(w, req, nil)
	assert.Nil(t, err)

	// check for returned error
	err = ff(w, req, nil)
	assert.Equal(t, errors.New("Internal Server Error").Error(), err.Error())

	// timeout
	ft := d(timeout)
	router.GET("/timeout", middleware.UseMiddleware(ft))
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/timeout", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusServiceUnavailable, w.Code)

	// check for panic
	fp := d(panicAttack)
	router.GET("/panic", middleware.UseMiddleware(fp))
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/panic", nil)
	assert.Panics(t, func() { router.ServeHTTP(w, req) })
}

func TestWithRateLimit(t *testing.T) {
	d := middleware.WithRateLimit(1, 5)
	router := httprouter.New()

	fs := d(success)
	router.GET("/success", middleware.UseMiddleware(fs))

	// 5 burst
	for i := 0; i < 5; i++ {
		req, _ := http.NewRequest("GET", "/success", nil)
		w := httptest.NewRecorder()
		router.ServeHTTP(w, req)
		assert.Equal(t, http.StatusOK, w.Code)
	}

	// too  many request at 6
	w2 := httptest.NewRecorder()
	req2, _ := http.NewRequest("GET", "/success", nil)
	router.ServeHTTP(w2, req2)
	assert.Equal(t, http.StatusTooManyRequests, w2.Code)

	// can handle other service
	w3 := httptest.NewRecorder()
	req3, _ := http.NewRequest("GET", "/success", nil)
	req3.Header.Set("Authorization", "haha")
	router.ServeHTTP(w3, req3)
	assert.Equal(t, http.StatusOK, w3.Code)

	// refill token
	time.Sleep(1 * time.Second)
	req4, _ := http.NewRequest("GET", "/success", nil)
	w4 := httptest.NewRecorder()
	router.ServeHTTP(w4, req4)
	assert.Equal(t, http.StatusOK, w4.Code)
}

func TestWithHTTPStatusInstrumentation(t *testing.T) {
	type args struct {
		handler middleware.HandleWithError
		method  string
		path    string
	}
	type expected struct {
		method     string
		path       string
		status     string
		httpStatus int
	}
	tests := []struct {
		name     string
		args     args
		expected expected
	}{
		{
			name: "200",
			args: args{
				handler: success,
				method:  "GET",
				path:    "/success",
			},
			expected: expected{
				method:     "GET",
				path:       "/success",
				status:     "ok",
				httpStatus: 200,
			},
		},
		{
			name: "200 already wrapped",
			args: args{
				handler: success,
				method:  "GET",
				path:    "/success",
			},
			expected: expected{
				method:     "GET",
				path:       "/success",
				status:     "ok",
				httpStatus: 200,
			},
		},
		{
			name: "500",
			args: args{
				handler: fail,
				method:  "POST",
				path:    "/fail",
			},
			expected: expected{
				method:     "POST",
				path:       "/fail",
				status:     "fail",
				httpStatus: 500,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			observer := &myObserver{}
			d := middleware.WithHTTPStatusInstrumentation(observer)
			router := httprouter.New()

			fs := d(tt.args.handler)
			router.Handle(tt.args.method, tt.args.path, middleware.UseMiddleware(fs))
			w := httptest.NewRecorder()
			req, _ := http.NewRequest(tt.args.method, tt.args.path, nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.expected.httpStatus, w.Code)
			assert.Equal(t, tt.expected.method, observer.method)
			assert.Equal(t, tt.expected.path, observer.path)
			assert.Equal(t, tt.expected.status, observer.status)
		})
	}
}

func TestWithStandardContext(t *testing.T) {
	d := middleware.WithStandardContext()
	router := httprouter.New()

	// success
	fs := d(success)
	router.GET("/success", middleware.UseMiddleware(fs))
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/success", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)

	// fail
	ff := d(fail)
	router.GET("/fail", middleware.UseMiddleware(ff))
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/fail", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)

	// already has req-id
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/success", nil)
	req.Header.Set("X-Request-ID", "kmzwa8awaa")
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestWithLogging(t *testing.T) {
	l, _ := zap.NewProduction(zap.AddStacktrace(zapcore.FatalLevel + 1))
	logger := log.NewCustomLogger(l)
	d := middleware.WithLogging(logger, &MyContextResolver{})
	router := httprouter.New()

	// success
	fs := d(success)
	router.GET("/success", middleware.UseMiddleware(fs))
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/success", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)

	// fail
	ff := d(fail)
	router.GET("/fail", middleware.UseMiddleware(ff))
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/fail", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)

	ffstd := d(failStd)
	router.GET("/failstd", middleware.UseMiddleware(ffstd))
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/failstd", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func TestWithTracing(t *testing.T) {
	d := middleware.WithTracing(opentracing.NoopTracer{})
	router := httprouter.New()

	// success
	fs := d(success)
	router.GET("/success", middleware.UseMiddleware(fs))
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/success", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)

	// fail
	ff := d(fail)
	router.GET("/fail", middleware.UseMiddleware(ff))
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/fail", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)

	// make it as SpanContext found
	fs = d(success)
	router.GET("/success-with-span-context", middleware.UseMiddleware(fs))
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/success-with-span-context", nil)

	span := opentracing.StartSpan("/success-with-span-context")
	defer span.Finish()

	span.Tracer().Inject(span.Context(),
		opentracing.TextMap,
		opentracing.HTTPHeadersCarrier(req.Header),
	)

	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)
}

func success(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	w.WriteHeader(http.StatusOK)
	return nil
}

func errorCauseFunc() error {
	return errors.New("Internal Server Error")
}

func fail(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	w.WriteHeader(http.StatusInternalServerError)
	return errorCauseFunc()
}

func timeout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	time.Sleep(500 * time.Millisecond)
	return nil
}

func panicAttack(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	panic("this is panic attack")
}

func returnNil(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	return nil
}

type myObserver struct {
	latency    float64
	method     string
	path       string
	status     string
	httpStatus int
}

func (obs *myObserver) Observe(_ context.Context, latency float64, method, path, status string, httpStatus int) {
	obs.latency = latency
	obs.method = method
	obs.path = path
	obs.status = status
	obs.httpStatus = httpStatus
}

type MyContextResolver struct{}

func (m *MyContextResolver) Metadata(_ context.Context) map[string]interface{} {
	return map[string]interface{}{
		"user_id": 1,
	}
}

func failStd(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	w.WriteHeader(http.StatusInternalServerError)
	return fmt.Errorf("Internal Server Error")
}
