package router

import (
	"github.com/julienschmidt/httprouter"
	"gitlab.com/rizaldntr/cards/pkg/middleware"
)

type IHandler interface {
	Register(router *httprouter.Router, middleware ...middleware.Middleware)
}

type IRouter interface {
	WithHandler(handler IHandler) *Router
}

type Router struct {
	router *httprouter.Router
}

func NewRouter() *Router {
	return &Router{
		router: httprouter.New(),
	}
}

func (r *Router) WithHandler(handler IHandler, middlewares ...middleware.Middleware) *Router {
	handler.Register(r.router, middlewares...)

	return r
}

func (r *Router) Get() *httprouter.Router {
	return r.router
}
