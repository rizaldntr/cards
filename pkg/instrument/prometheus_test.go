package instrument_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/pkg/instrument"
)

func TestHistogramSingleton(t *testing.T) {
	assert.Equal(t, instrument.NewHistogram(), instrument.NewHistogram())
}

func TestObserveLatency(t *testing.T) {
	assert.NotPanics(t,
		func() {
			instrument.NewHistogram().Observe(context.TODO(), 0.03, "GET", "/cards", "ok", 200)
		})
}
