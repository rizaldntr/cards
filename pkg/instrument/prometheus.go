// Package instrument is used to anything related to instrumentation.
// By default, it uses Prometheus.
package instrument

import (
	"context"
	"strconv"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	// histogram is histogram metric
	histogram *Histogram
	// for instantiate histogram singleton
	hOnce sync.Once
)

// Observe is a method to write metric, especially service latency, to prometheus using histogram type.
// The name of the metric is `service_latency_seconds`.
// It has three labels: `method`, `action`, and `status`.
//
// Label method is used to identify which method the metric belongs to,
// such as HTTP GET or HTTP POST.
// Label action is used to identify which action the metric belongs to,
// such as function name or HTTP path.
// Label status should only have one of these two values, "ok" or "fail".
// Status "ok" indicates that the function behaves well.
// Status "fail" indicates that there is an error occurred during the process.
//
// The last parameter is `latency`.
// It indicates the running time of a function.
func (h *Histogram) Observe(ctx context.Context, latency float64, method, path, status string, httpStatus int) {
	h.metric.WithLabelValues(method, path, status, strconv.Itoa(httpStatus)).Observe(latency)
}

func newHistogramVec() *prometheus.HistogramVec {
	return prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "service_latency_seconds",
		Help: "service latency in seconds",
	}, []string{"method", "action", "status", "http_status"})
}

// Histogram is a struct that holds histogram metric
type Histogram struct {
	metric *prometheus.HistogramVec
}

// NewHistogram creates new Histogram instance
func NewHistogram() *Histogram {
	hOnce.Do(func() {
		histogram = &Histogram{newHistogramVec()}
		prometheus.MustRegister(histogram.metric)
	})

	return histogram
}
