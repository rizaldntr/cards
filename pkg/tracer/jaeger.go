package tracer

import (
	"io"

	"github.com/opentracing/opentracing-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	jaegerlog "github.com/uber/jaeger-client-go/log"
	"github.com/uber/jaeger-lib/metrics"
	"gitlab.com/rizaldntr/cards/internal/config"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

func Get(config *config.Config, serviceName string) (opentracing.Tracer, io.Closer) {
	cfg := jaegercfg.Configuration{
		ServiceName: serviceName,
		Sampler: &jaegercfg.SamplerConfig{
			Type:              config.JeagerConfig.SamplingType,
			Param:             config.JeagerConfig.SamplingValue,
			SamplingServerURL: config.JeagerConfig.SamplingServerURL,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans:           true,
			LocalAgentHostPort: config.JeagerConfig.LocalAgentAddress,
		},
	}

	jLogger := jaegerlog.StdLogger
	jMetricsFactory := metrics.NullFactory

	// Initialize tracer with a logger and a metrics factory
	tracer, closer, err := cfg.NewTracer(
		jaegercfg.Logger(jLogger),
		jaegercfg.Metrics(jMetricsFactory),
	)
	if err != nil {
		log.Error.Fatal(err.Error())
	}

	return tracer, closer
}
