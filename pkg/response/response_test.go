package response_test

import (
	"net/http"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

func TestBuildSuccess(t *testing.T) {
	data := "data"
	meta := "meta"

	res := response.BuildSuccess(data, "request accepted", meta)
	assert.NotNil(t, res)
}

func TestBuildError(t *testing.T) {
	res := response.BuildError()
	assert.NotNil(t, res)

	res = response.BuildError(errors.New("Error"))
	assert.NotNil(t, res)

	ce := response.CustomError{}
	res = response.BuildError(ce)
	assert.NotNil(t, res)

	errInfos := []error{
		response.InfoError{
			Message: "message",
			Code:    10123,
			Field:   "transaction_id",
		},
		response.InfoError{
			Message: "message",
			Code:    10123,
			Field:   "user_id",
		},
	}

	res = response.BuildError(errInfos...)
	assert.NotNil(t, res)
	assert.Equal(t, 2, len(res.Errors))

	err := response.InternalServerErrorBody()
	res = response.BuildError(err)
	assert.NotNil(t, res)
	assert.Equal(t, err, res)

	errResp := response.ResponseError{
		ErrorBody:  err,
		HTTPStatus: 500,
	}

	res = response.BuildError(errResp)
	assert.NotNil(t, res)
	assert.Equal(t, err, res)
}

type Writer struct {
	Status int
}

func (w *Writer) Header() http.Header {
	return make(http.Header)
}

func (w *Writer) Write(b []byte) (int, error) {
	return 0, nil
}

func (w *Writer) WriteHeader(status int) {
	w.Status = status
}

func TestWrite(t *testing.T) {
	writer := &Writer{}
	assert.NotPanics(t, func() { response.Write(writer, "data", 200) })
}

func TestCustomError_Error(t *testing.T) {
	ce := response.CustomError{Message: "wumpeda"}
	assert.Equal(t, ce.Message, ce.Error())
}

func TestErrorBody_Error(t *testing.T) {
	ce := response.BodyError{
		Errors: []response.InfoError{
			{
				Message: "message",
				Code:    10123,
				Field:   "transaction_id",
			},
		},
	}
	expected := `response - errors
	error - msg: message, code: 10123, field: transaction_id`
	assert.Equal(t, expected, ce.Error())
}

func TestErrorWithMeta(t *testing.T) {
	w := &Writer{}

	errWithMeta := response.BodyError{
		Errors: []response.InfoError{},
		Meta: response.MetaInfo{
			HTTPStatus: 500,
		},
	}
	response.Error(w, errWithMeta)
	assert.Equal(t, 500, w.Status)
}
