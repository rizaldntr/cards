package response

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
)

const (
	// ErrorCodeUnexpectedError Error code for unexpected error
	ErrorCodeUnexpectedError = 10000
)

// ErrUnexpectedError custom error on unexpected error
var ErrUnexpectedError = CustomError{
	Message:  "Unexpected error",
	Code:     ErrorCodeUnexpectedError,
	HTTPCode: http.StatusInternalServerError,
	Err:      errors.New("unexpected error"),
}

// SuccessBody holds data for success response
type SuccessBody struct {
	Data    interface{} `json:"data,omitempty"`
	Message string      `json:"message,omitempty"`
	Meta    interface{} `json:"meta"`
}

// BodyError holds data for error response
type BodyError struct {
	Errors []InfoError `json:"errors"`
	Meta   interface{} `json:"meta"`
}

func (e BodyError) Error() string {
	errMsg := "response - errors"
	for _, err := range e.Errors {
		errMsg += fmt.Sprintf("\n\t%s", err.Error())
	}

	return errMsg
}

//nolint:golint,revive
// ResponseError error from http.Response
type ResponseError struct {
	ErrorBody  BodyError
	HTTPStatus int
}

func (e ResponseError) Error() string {
	errMsg := e.ErrorBody.Error()
	errMsg += fmt.Sprintf("\nhttp_status: %d", e.HTTPStatus)

	return errMsg
}

// MetaInfo holds meta data
type MetaInfo struct {
	HTTPStatus int `json:"httpStatus"`
}

// InfoError holds error detail
type InfoError struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Field   string `json:"field,omitempty"`
}

// Error implement error
func (e InfoError) Error() string {
	return fmt.Sprintf(
		"error - msg: %s, code: %d, field: %s",
		e.Message,
		e.Code,
		e.Field,
	)
}

// CustomError holds data for customized error
type CustomError struct {
	Message  string
	Field    string
	Code     int
	HTTPCode int
	Err      error
}

// Error is a function to convert error to string.
// It exists to satisfy error interface
func (c CustomError) Error() string {
	return c.Message
}

// BuildSuccess is a function to create SuccessBody
func BuildSuccess(data interface{}, message string, meta interface{}) SuccessBody {
	return SuccessBody{
		Data:    data,
		Message: message,
		Meta:    meta,
	}
}

// BuildError is a function to create BodyError
func BuildError(errors ...error) BodyError {
	if len(errors) == 0 {
		return InternalServerErrorBody()
	}

	errInfos := []InfoError{}
	metaInfo := MetaInfo{}

	for _, err := range errors {
		switch errOrig := err.(type) {
		case CustomError:
			metaInfo = MetaInfo{
				HTTPStatus: errOrig.HTTPCode,
			}
			errInfos = append(errInfos, InfoError{
				Message: errOrig.Message,
				Code:    errOrig.Code,
				Field:   errOrig.Field,
			})
		case InfoError:
			errInfos = append(errInfos, errOrig)
		case BodyError:
			return errOrig
		case ResponseError:
			return errOrig.ErrorBody
		default:
			return InternalServerErrorBody()
		}
	}

	return BodyError{
		Errors: errInfos,
		Meta:   metaInfo,
	}
}

// InternalServerErrorBody for default internal server error
func InternalServerErrorBody() BodyError {
	return BodyError{
		Errors: []InfoError{
			{
				Message: ErrUnexpectedError.Message,
				Code:    ErrUnexpectedError.Code,
				Field:   ErrUnexpectedError.Field,
			},
		},
		Meta: MetaInfo{
			HTTPStatus: ErrUnexpectedError.HTTPCode,
		},
	}
}

// Write is a function to write data in json format
func Write(w http.ResponseWriter, result interface{}, status int) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	return json.NewEncoder(w).Encode(result)
}

// OK wraps success responses
func OK(w http.ResponseWriter, data interface{}, message string) {
	successResponse := BuildSuccess(data, message, MetaInfo{HTTPStatus: http.StatusOK})
	Write(w, successResponse, http.StatusOK)
}

// Created wrap create response
func Created(w http.ResponseWriter, data interface{}) {
	successResponse := BuildSuccess(data, "Created", MetaInfo{HTTPStatus: http.StatusCreated})
	Write(w, successResponse, http.StatusCreated)
}

// Error wraps error response
func Error(w http.ResponseWriter, err ...error) {
	errorResponse := BuildError(err...)

	meta, ok := errorResponse.Meta.(MetaInfo)
	if !ok {
		Write(w, errorResponse, 422)

		return
	}

	Write(w, errorResponse, meta.HTTPStatus)
}
