package db

import (
	"database/sql"

	// Needed to connect to psql
	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	gormopentracing "gorm.io/plugin/opentracing"
)

type DB struct {
	Client *gorm.DB
}

func Get(connStr string) (*DB, error) {
	sqlDB, err := get(connStr)
	if err != nil {
		return nil, err
	}

	db, err := gorm.Open(
		postgres.New(postgres.Config{
			Conn: sqlDB,
		}), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	db.Use(gormopentracing.New())

	return &DB{
		Client: db,
	}, nil
}

func (d *DB) Close() error {
	sqlDB, err := d.Client.DB()
	if err != nil {
		return err
	}

	return sqlDB.Close()
}

func get(connStr string) (*sql.DB, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
