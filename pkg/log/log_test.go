package log_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"go.uber.org/zap"
)

func TestRequest(t *testing.T) {
	logger := log.NewLogger()
	err := logger.Request("message",
		log.NewField("request_id", "KMZ-WA-8-AWAA"),
		log.NewField("duration", "231"),
		log.NewField("tags", `["info","success"]`),
	)
	assert.Nil(t, err)

	err = logger.Request("message",
		log.NewField("request_id", "KMZ-WA-8-AWAA"),
		log.NewField("tags", `["error","no duration"]`),
	)
	assert.NotNil(t, err)
}

func TestInfo(t *testing.T) {
	logger := log.NewLogger()

	assert.NotPanics(t, func() {
		logger.Info(
			"info",
			log.NewField("something", "example"),
		)
	})
}

func TestError(t *testing.T) {
	logger := log.NewLogger()

	assert.NotPanics(t, func() {
		logger.Error(
			"error",
			log.NewField("something", "an error"),
		)
	})
}

func TestWarn(t *testing.T) {
	logger := log.NewLogger()

	assert.NotPanics(t, func() {
		logger.Warn(
			"warn message",
			log.NewField("something", "warning input"),
		)
	})
}

func TestNewCustomLogger(t *testing.T) {
	l, _ := zap.NewProduction(zap.AddCallerSkip(1))
	logger := log.NewCustomLogger(l)

	assert.NotNil(t, logger)
}
