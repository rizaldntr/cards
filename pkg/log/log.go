// Package log is used to write logs to stdout.
package log

import (
	"errors"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Logger represents the object that will do the logging event.
type Logger struct {
	logger *zap.Logger
}

// Field holds key-value to be written to log.
type Field struct {
	Key   string
	Value interface{}
}

// NewLogger creates new Logger instance.
func NewLogger() *Logger {
	log, _ := zap.NewProduction(zap.AddCallerSkip(1))

	return &Logger{
		logger: log,
	}
}

// NewCustomLogger creates new Logger instance with custom zap log.
func NewCustomLogger(logger *zap.Logger) *Logger {
	return &Logger{
		logger: logger,
	}
}

// NewField returns Field with given key and value.
func NewField(key string, value interface{}) Field {
	return Field{key, value}
}

// Request writes log with severity = info.
// It will only write log if mandatory fields are given.
// Otherwise, it will return an error.
func (l *Logger) Request(message string, fields ...Field) error {
	if zapFields, proceed := convertAndCheckFields(fields...); proceed && message != "" {
		l.logger.Info(message, zapFields...)

		return nil
	}

	return errors.New("fields don't contain all mandatory fields")
}

// Info writes log with severity = info.
func (l *Logger) Info(message string, fields ...Field) {
	fields = append(fields, NewField("severity", "info"))
	zapFields := convertFields(fields...)
	l.logger.Info(message, zapFields...)
}

// Warn writes log with severity = warn.
func (l *Logger) Warn(message string, fields ...Field) {
	fields = append(fields, NewField("severity", "warn"))
	zapFields := convertFields(fields...)
	l.logger.Warn(message, zapFields...)
}

// Error writes log with severity = error.
func (l *Logger) Error(message string, fields ...Field) {
	fields = append(fields, NewField("severity", "error"))
	zapFields := convertFields(fields...)
	l.logger.Error(message, zapFields...)
}

func convertAndCheckFields(fields ...Field) ([]zapcore.Field, bool) {
	var flag uint
	var zapFields []zapcore.Field

	for _, field := range fields {
		if field.Key == "request_id" {
			flag = flag | 1
		} else if field.Key == "tags" {
			flag = flag | 2
		} else if field.Key == "duration" {
			flag = flag | 4
		}

		zapFields = append(zapFields, zap.Any(field.Key, field.Value))
	}

	return zapFields, flag == 7
}

func convertFields(fields ...Field) []zapcore.Field {
	var zapFields []zapcore.Field
	for _, field := range fields {
		zapFields = append(zapFields, zap.Any(field.Key, field.Value))
	}

	return zapFields
}
