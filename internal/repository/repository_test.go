package repository_test

import (
	"reflect"
	"testing"

	"gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/repository"
	"gitlab.com/rizaldntr/cards/pkg/response"
	"gorm.io/gorm"
)

func Test_handleDBError(t *testing.T) {
	tests := []struct {
		name      string
		gormError error
		wantErr   response.CustomError
	}{
		{
			name:      "gorm.ErrRecordNotFound should return ErrNotFound",
			gormError: gorm.ErrRecordNotFound,
			wantErr:   errors.ErrRecordNotFound,
		},
		{
			name:      "gorm.ErrInvalidData should return ErrInvalidParameters",
			gormError: gorm.ErrInvalidData,
			wantErr:   errors.ErrInvalidParameters,
		},
		{
			name:      "gorm.ErrInvalidData should return ErrInvalidParameters",
			gormError: gorm.ErrInvalidField,
			wantErr:   errors.ErrInvalidParameters,
		},
		{
			name:      "gorm.ErrInvalidDB should return ErrUnexpectedError",
			gormError: gorm.ErrInvalidDB,
			wantErr:   response.ErrUnexpectedError,
		},
		{
			name:      "gorm.ErrInvalidTransaction should return ErrUnexpectedError",
			gormError: gorm.ErrInvalidTransaction,
			wantErr:   response.ErrUnexpectedError,
		},
		{
			name:      "gorm.ErrInvalidValueOfLength should return ErrInvalidParameters",
			gormError: gorm.ErrInvalidValueOfLength,
			wantErr:   errors.ErrInvalidParameters,
		},
		{
			name:      "gorm.ErrInvalidValue should return ErrInvalidParameters",
			gormError: gorm.ErrInvalidValue,
			wantErr:   errors.ErrInvalidParameters,
		},
		{
			name:      "gorm.ErrPrimaryKeyRequired should return ErrInvalidParameters",
			gormError: gorm.ErrPrimaryKeyRequired,
			wantErr:   errors.ErrInvalidParameters,
		},
		{
			name:      "gorm.ErrEmptySlice should return ErrUnexpectedError as default",
			gormError: gorm.ErrEmptySlice,
			wantErr:   response.ErrUnexpectedError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := repository.HandleDBError(&gorm.DB{Error: tt.gormError}); !reflect.DeepEqual(err, tt.wantErr) {
				t.Errorf("handleDBError() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
