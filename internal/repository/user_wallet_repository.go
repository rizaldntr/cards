package repository

import (
	"context"

	"github.com/shopspring/decimal"
	"gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type UserWalletRepository struct {
	db     *db.DB
	logger *log.Logger
}

func NewUserWalletRepository(db *db.DB, logger *log.Logger) *UserWalletRepository {
	return &UserWalletRepository{
		db:     db,
		logger: logger,
	}
}

func (uw *UserWalletRepository) Create(ctx context.Context, userWallet *model.UserWallet) error {
	result := uw.db.Client.WithContext(ctx).Create(userWallet)

	if err := handleDBError(result); err != nil {
		return err
	}

	return nil
}

func (uw *UserWalletRepository) Update(ctx context.Context, id uint64, userWallet *model.UserWallet) error {
	result := uw.db.Client.WithContext(ctx).Model(&model.UserWallet{}).Where("id = ?", id).Updates(userWallet)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}

func (uw *UserWalletRepository) Read(ctx context.Context, id uint64) (*model.UserWallet, error) {
	userWallet := &model.UserWallet{}
	result := uw.db.Client.WithContext(ctx).First(userWallet, id)

	if err := handleDBError(result); err != nil {
		return nil, err
	}

	return userWallet, nil
}

func (uw *UserWalletRepository) Delete(ctx context.Context, id uint64) error {
	result := uw.db.Client.WithContext(ctx).Delete(&model.UserWallet{}, id)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}

func (uw *UserWalletRepository) Topup(ctx context.Context, id uint64, amount decimal.Decimal) error {
	err := uw.db.Client.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		wallet := model.UserWallet{}
		if err := tx.Clauses(clause.Locking{Strength: "UPDATE"}).First(&wallet, id).Error; err != nil {
			return gorm.ErrInvalidTransaction
		}

		// Update balance
		if err := tx.Model(wallet).Update("balance", gorm.Expr("balance + ?", amount)).Error; err != nil {
			return gorm.ErrInvalidTransaction
		}

		return nil
	})
	if err != nil {
		return errors.ErrUnprocessableEntity
	}

	return nil
}
