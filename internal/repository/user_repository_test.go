package repository_test

import (
	"context"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/repository"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type userRepositoryTestSuite struct {
	suite.Suite
	db     *db.DB
	mock   sqlmock.Sqlmock
	logger *log.Logger

	app *repository.UserRepository
}

func (t *userRepositoryTestSuite) SetupTest() {
	pql, mock, _ := sqlmock.New()
	gormDB, _ := gorm.Open(postgres.New(postgres.Config{Conn: pql}), &gorm.Config{})

	t.db = &db.DB{Client: gormDB}
	t.mock = mock
	t.logger = &log.Logger{}
	t.app = repository.NewUserRepository(t.db, t.logger)
}

func (t *userRepositoryTestSuite) TestNewUserRepository() {
	teamWalletRepository := repository.NewUserRepository(&db.DB{}, &log.Logger{})
	assert.NotNil(t.T(), teamWalletRepository)
}

func (t *userRepositoryTestSuite) TestUserRepository_Delete() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "users" WHERE "users"."id" = $1`)).
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()
	err := t.app.Delete(context.Background(), 1)
	assert.Nil(t.T(), err)
}

func (t *userRepositoryTestSuite) TestUserRepository_DeleteError() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "users" WHERE "users"."id" = $1`)).
		WithArgs(1).
		WillReturnError(errors.New("Error"))
	t.mock.ExpectRollback()
	err := t.app.Delete(context.Background(), 1)
	assert.NotNil(t.T(), err)
}

func (t *userRepositoryTestSuite) TestUserRepository_Read() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "users" WHERE "users"."id" = $1 ORDER BY "users"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	user, err := t.app.Read(context.Background(), 1)
	assert.Nil(t.T(), err)
	assert.NotNil(t.T(), user)
}

func (t *userRepositoryTestSuite) TestUserRepository_ReadError() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "users" WHERE "users"."id" = $1 ORDER BY "users"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnError(errors.New("Not found"))
	user, err := t.app.Read(context.Background(), 1)
	assert.NotNil(t.T(), err)
	assert.Nil(t.T(), user)
}

func (t *userRepositoryTestSuite) TestUserRepository_Update() {
	user := mockModelUser()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "users" SET "updated_at"=$1,"name"=$2,"username"=$3,"password"=$4,"email"=$5 WHERE id = $6`)).
		WithArgs(sqlmock.AnyArg(), user.Name, user.Username, user.Password, user.Email, 1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()

	err := t.app.Update(context.Background(), 1, user)
	assert.Nil(t.T(), err)
	assert.NotNil(t.T(), user.ID)
}

func (t *userRepositoryTestSuite) TestUserRepository_UpdateError() {
	user := mockModelUser()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "users" SET "updated_at"=$1,"name"=$2,"username"=$3,"password"=$4,"email"=$5 WHERE id = $6`)).
		WithArgs(sqlmock.AnyArg(), user.Name, user.Username, user.Password, user.Email, 1).
		WillReturnError(errors.New("Resource not found"))
	t.mock.ExpectRollback()

	err := t.app.Update(context.Background(), 1, user)
	assert.NotNil(t.T(), err)
}

func (t *userRepositoryTestSuite) TestUserRepository_Create() {
	user := mockModelUser()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "users" ("created_at","updated_at","name","username","password","email") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "name","username","password","phone","email","active","id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), user.Name, user.Username, user.Password, user.Email).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	t.mock.ExpectCommit()

	err := t.app.Create(context.Background(), user)
	assert.Nil(t.T(), err)
	assert.Equal(t.T(), user.ID, uint64(1))
}

func (t *userRepositoryTestSuite) TestUserRepository_CreateError() {
	user := mockModelUser()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "users" ("created_at","updated_at","name","username","password","email") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "name","username","password","phone","email","active","id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), user.Name, user.Username, user.Password, user.Email).
		WillReturnError(errors.New("duplicate id"))
	t.mock.ExpectRollback()

	err := t.app.Create(context.Background(), user)
	assert.Equal(t.T(), user.ID, uint64(0))
	assert.NotNil(t.T(), err)
}

func TestUserRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(userRepositoryTestSuite))
}

func mockModelUser() *model.User {
	return &model.User{
		Name:     "John doe",
		Username: "john_D",
		Email:    "johndoe@gmail.com",
		Password: "veryStrongP4s5W0rD",
	}
}
