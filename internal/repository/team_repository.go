package repository

import (
	"context"

	"gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

type TeamRepository struct {
	db     *db.DB
	logger *log.Logger
}

func NewTeamRepository(db *db.DB, logger *log.Logger) *TeamRepository {
	return &TeamRepository{
		db:     db,
		logger: logger,
	}
}

func (t *TeamRepository) Create(ctx context.Context, team *model.Team) error {
	result := t.db.Client.WithContext(ctx).Create(team)

	if result.Error != nil {
		return response.ErrUnexpectedError
	}

	return nil
}

func (t *TeamRepository) Update(ctx context.Context, id uint64, team *model.Team) error {
	result := t.db.Client.WithContext(ctx).WithContext(ctx).Model(&model.Team{}).Where("id = ?", id).Updates(team)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}

func (t *TeamRepository) Read(ctx context.Context, id uint64) (*model.Team, error) {
	team := &model.Team{}
	result := t.db.Client.WithContext(ctx).First(team, id)

	if err := handleDBError(result); err != nil {
		return nil, err
	}

	return team, nil
}

func (t *TeamRepository) Delete(ctx context.Context, id uint64) error {
	result := t.db.Client.WithContext(ctx).Delete(&model.Team{}, id)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}
