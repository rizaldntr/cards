package repository_test

import (
	"context"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/repository"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type userWalletRepositoryTestSuite struct {
	suite.Suite
	db     *db.DB
	mock   sqlmock.Sqlmock
	logger *log.Logger

	app *repository.UserWalletRepository
}

func (t *userWalletRepositoryTestSuite) SetupTest() {
	pql, mock, _ := sqlmock.New()
	gormDB, _ := gorm.Open(postgres.New(postgres.Config{Conn: pql}), &gorm.Config{})

	t.db = &db.DB{Client: gormDB}
	t.mock = mock
	t.logger = &log.Logger{}
	t.app = repository.NewUserWalletRepository(t.db, t.logger)
}

func (t *userWalletRepositoryTestSuite) TestNewUserWalletRepository() {
	teamWalletRepository := repository.NewUserWalletRepository(&db.DB{}, &log.Logger{})
	assert.NotNil(t.T(), teamWalletRepository)
}

func (t *userWalletRepositoryTestSuite) TestUserWalletRepository_Delete() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "user_wallets" WHERE "user_wallets"."id" = $1`)).
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()
	err := t.app.Delete(context.Background(), 1)
	assert.Nil(t.T(), err)
}

func (t *userWalletRepositoryTestSuite) TestUserWalletRepository_DeleteError() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "user_wallets" WHERE "user_wallets"."id" = $1`)).
		WithArgs(1).
		WillReturnError(errors.New("Error"))
	t.mock.ExpectRollback()
	err := t.app.Delete(context.Background(), 1)
	assert.NotNil(t.T(), err)
}

func (t *userWalletRepositoryTestSuite) TestUserWalletRepository_Read() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "user_wallets" WHERE "user_wallets"."id" = $1 ORDER BY "user_wallets"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	user, err := t.app.Read(context.Background(), 1)
	assert.Nil(t.T(), err)
	assert.NotNil(t.T(), user)
}

func (t *userWalletRepositoryTestSuite) TestUserWalletRepository_ReadError() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "user_wallets" WHERE "user_wallets"."id" = $1 ORDER BY "user_wallets"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnError(errors.New("Not found"))
	user, err := t.app.Read(context.Background(), 1)
	assert.NotNil(t.T(), err)
	assert.Nil(t.T(), user)
}

func (t *userWalletRepositoryTestSuite) TestUserWalletRepository_Update() {
	userWallet := mockModelUserWallet()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "user_wallets" SET "user_id"=$1,"name"=$2,"updated_at"=$3 WHERE id = $4`)).
		WithArgs(userWallet.UserID, userWallet.Name, sqlmock.AnyArg(), 1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()

	err := t.app.Update(context.Background(), 1, userWallet)
	assert.Nil(t.T(), err)
}

func (t *userWalletRepositoryTestSuite) TestUserWalletRepository_UpdateError() {
	userWallet := mockModelUserWallet()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "user_wallets" SET "user_id"=$1,"name"=$2,"updated_at"=$3 WHERE id = $4`)).
		WithArgs(userWallet.UserID, userWallet.Name, sqlmock.AnyArg(), 1).
		WillReturnError(errors.New("Resource not found"))
	t.mock.ExpectRollback()

	err := t.app.Update(context.Background(), 1, userWallet)
	assert.NotNil(t.T(), err)
}

func (t *userWalletRepositoryTestSuite) TestUserWalletRepository_Create() {
	userWallet := mockModelUserWallet()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "user_wallets" ("created_at","updated_at","user_id","name") VALUES ($1,$2,$3,$4) RETURNING "user_id","name","balance","active","id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), userWallet.UserID, userWallet.Name).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	t.mock.ExpectCommit()

	err := t.app.Create(context.Background(), userWallet)
	assert.Nil(t.T(), err)
	assert.Equal(t.T(), userWallet.ID, uint64(1))
}

func (t *userWalletRepositoryTestSuite) TestUserWalletRepository_CreateError() {
	userWallet := mockModelUserWallet()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "user_wallets" ("created_at","updated_at","user_id","name") VALUES ($1,$2,$3,$4) RETURNING "user_id","name","balance","active","id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), userWallet.UserID, userWallet.Name).
		WillReturnError(errors.New("duplicate id"))
	t.mock.ExpectRollback()

	err := t.app.Create(context.Background(), userWallet)
	assert.NotNil(t.T(), err)
}

func TestUserWalletRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(userWalletRepositoryTestSuite))
}

func mockModelUserWallet() *model.UserWallet {
	return &model.UserWallet{
		UserID: 1,
		Name:   "John doe",
	}
}
