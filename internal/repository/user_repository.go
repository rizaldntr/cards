package repository

import (
	"context"

	"gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

type UserRepository struct {
	db     *db.DB
	logger *log.Logger
}

func NewUserRepository(db *db.DB, logger *log.Logger) *UserRepository {
	return &UserRepository{
		db:     db,
		logger: logger,
	}
}

func (u *UserRepository) Create(ctx context.Context, user *model.User) error {
	result := u.db.Client.WithContext(ctx).Create(user)

	if err := handleDBError(result); err != nil {
		return err
	}

	return nil
}

func (u *UserRepository) Update(ctx context.Context, id uint64, user *model.User) error {
	result := u.db.Client.WithContext(ctx).Model(&model.User{}).Where("id = ?", id).Updates(user)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}

func (u *UserRepository) Read(ctx context.Context, id uint64) (*model.User, error) {
	user := &model.User{}
	result := u.db.Client.WithContext(ctx).First(user, id)

	if err := handleDBError(result); err != nil {
		return nil, err
	}

	return user, nil
}

func (u *UserRepository) Delete(ctx context.Context, id uint64) error {
	result := u.db.Client.WithContext(ctx).Delete(&model.User{}, id)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}
