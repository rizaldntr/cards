package repository_test

import (
	"context"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/repository"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type teamRepositoryTestSuite struct {
	suite.Suite
	db     *db.DB
	mock   sqlmock.Sqlmock
	logger *log.Logger

	app *repository.TeamRepository
}

func (t *teamRepositoryTestSuite) SetupTest() {
	pql, mock, _ := sqlmock.New()
	gormDB, _ := gorm.Open(postgres.New(postgres.Config{Conn: pql}), &gorm.Config{})

	t.db = &db.DB{Client: gormDB}
	t.mock = mock
	t.logger = &log.Logger{}
	t.app = repository.NewTeamRepository(t.db, t.logger)
}

func (t *teamRepositoryTestSuite) TestNewTeamRepository() {
	teamWalletRepository := repository.NewTeamRepository(&db.DB{}, &log.Logger{})
	assert.NotNil(t.T(), teamWalletRepository)
}

func (t *teamRepositoryTestSuite) TestTeamRepository_Delete() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "teams" WHERE "teams"."id" = $1`)).
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()
	err := t.app.Delete(context.Background(), 1)
	assert.Nil(t.T(), err)
}

func (t *teamRepositoryTestSuite) TestTeamRepository_DeleteError() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "teams" WHERE "teams"."id" = $1`)).
		WithArgs(1).
		WillReturnError(errors.New("Error"))
	t.mock.ExpectRollback()
	err := t.app.Delete(context.Background(), 1)
	assert.NotNil(t.T(), err)
}

func (t *teamRepositoryTestSuite) TestTeamRepository_Read() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "teams" WHERE "teams"."id" = $1 ORDER BY "teams"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	user, err := t.app.Read(context.Background(), 1)
	assert.Nil(t.T(), err)
	assert.NotNil(t.T(), user)
}

func (t *teamRepositoryTestSuite) TestTeamRepository_ReadError() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "teams" WHERE "teams"."id" = $1 ORDER BY "teams"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnError(errors.New("Not found"))
	user, err := t.app.Read(context.Background(), 1)
	assert.NotNil(t.T(), err)
	assert.Nil(t.T(), user)
}

func (t *teamRepositoryTestSuite) TestTeamRepository_Update() {
	team := mockModel()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "teams" SET "name"=$1,"updated_at"=$2 WHERE id = $3`)).
		WithArgs(team.Name, sqlmock.AnyArg(), 1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()

	err := t.app.Update(context.Background(), 1, team)
	assert.Nil(t.T(), err)
}

func (t *teamRepositoryTestSuite) TestTeamRepository_UpdateError() {
	team := mockModel()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "teams" SET "name"=$1,"updated_at"=$2 WHERE id = $3`)).
		WithArgs(team.Name, sqlmock.AnyArg(), 1).
		WillReturnError(errors.New("Resource not found"))
	t.mock.ExpectRollback()

	err := t.app.Update(context.Background(), 1, team)
	assert.NotNil(t.T(), err)
}

func (t *teamRepositoryTestSuite) TestTeamRepository_Create() {
	team := mockModel()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "teams" ("created_at","updated_at","name") VALUES ($1,$2,$3) RETURNING "name","active","id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), team.Name).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	t.mock.ExpectCommit()

	err := t.app.Create(context.Background(), team)
	assert.Nil(t.T(), err)
	assert.Equal(t.T(), team.ID, uint64(1))
}

func (t *teamRepositoryTestSuite) TestTeamRepository_CreateError() {
	team := mockModel()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "teams" ("created_at","updated_at","name") VALUES ($1,$2,$3) RETURNING "name","active","id"`)).
		WithArgs(team.Name, sqlmock.AnyArg(), sqlmock.AnyArg()).
		WillReturnError(errors.New("duplicate id"))
	t.mock.ExpectRollback()

	err := t.app.Create(context.Background(), team)
	assert.Equal(t.T(), team.ID, uint64(0))
	assert.NotNil(t.T(), err)
}

func TestTeamRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(teamRepositoryTestSuite))
}

func mockModel() *model.Team {
	return &model.Team{
		Name: "Spenmo",
	}
}
