package repository_test

import (
	"context"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/repository"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type teamWalletRepositoryTestSuite struct {
	suite.Suite
	db     *db.DB
	mock   sqlmock.Sqlmock
	logger *log.Logger

	app *repository.TeamWalletRepository
}

func (t *teamWalletRepositoryTestSuite) SetupTest() {
	pql, mock, _ := sqlmock.New()
	gormDB, _ := gorm.Open(postgres.New(postgres.Config{Conn: pql}), &gorm.Config{})

	t.db = &db.DB{Client: gormDB}
	t.mock = mock
	t.logger = &log.Logger{}
	t.app = repository.NewTeamWalletRepository(t.db, t.logger)
}

func (t *teamWalletRepositoryTestSuite) TestNewTeamWalletRepository() {
	teamWalletRepository := repository.NewTeamWalletRepository(&db.DB{}, &log.Logger{})
	assert.NotNil(t.T(), teamWalletRepository)
}

func (t *teamWalletRepositoryTestSuite) TestTeamWalletRepository_Delete() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "team_wallets" WHERE "team_wallets"."id" = $1`)).
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()
	err := t.app.Delete(context.Background(), 1)
	assert.Nil(t.T(), err)
}

func (t *teamWalletRepositoryTestSuite) TestTeamWalletRepository_DeleteError() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "team_wallets" WHERE "team_wallets"."id" = $1`)).
		WithArgs(1).
		WillReturnError(errors.New("Error"))
	t.mock.ExpectRollback()
	err := t.app.Delete(context.Background(), 1)
	assert.NotNil(t.T(), err)
}

func (t *teamWalletRepositoryTestSuite) TestTeamWalletRepository_Read() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "team_wallets" WHERE "team_wallets"."id" = $1 ORDER BY "team_wallets"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	user, err := t.app.Read(context.Background(), 1)
	assert.Nil(t.T(), err)
	assert.NotNil(t.T(), user)
}

func (t *teamWalletRepositoryTestSuite) TestTeamWalletRepository_ReadError() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "team_wallets" WHERE "team_wallets"."id" = $1 ORDER BY "team_wallets"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnError(errors.New("Not found"))
	user, err := t.app.Read(context.Background(), 1)
	assert.NotNil(t.T(), err)
	assert.Nil(t.T(), user)
}

func (t *teamWalletRepositoryTestSuite) TestTeamWalletRepository_Update() {
	teamWallet := mockModelTeamWallet()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "team_wallets" SET "name"=$1,"updated_at"=$2 WHERE id = $3`)).
		WithArgs(teamWallet.Name, sqlmock.AnyArg(), 1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()

	err := t.app.Update(context.Background(), 1, teamWallet)
	assert.Nil(t.T(), err)
}

func (t *teamWalletRepositoryTestSuite) TestTeamWalletRepository_UpdateError() {
	teamWallet := mockModelTeamWallet()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "teams" SET "name"=$1,"updated_at"=$2 WHERE id = $3`)).
		WithArgs(teamWallet.Name, sqlmock.AnyArg(), 1).
		WillReturnError(errors.New("Resource not found"))
	t.mock.ExpectRollback()

	err := t.app.Update(context.Background(), 1, teamWallet)
	assert.NotNil(t.T(), err)
	assert.Equal(t.T(), teamWallet.ID, uint64(0))
}

func (t *teamWalletRepositoryTestSuite) TestTeamWalletRepository_Create() {
	teamWallet := mockModelTeamWallet()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "team_wallets" ("created_at","updated_at","name") VALUES ($1,$2,$3) RETURNING "team_id","name","balance","active","daily_limit","daily_usage","monthly_limit","monthly_usage","id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), teamWallet.Name).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	t.mock.ExpectCommit()

	err := t.app.Create(context.Background(), teamWallet)
	assert.Nil(t.T(), err)
	assert.Equal(t.T(), teamWallet.ID, uint64(1))
}

func (t *teamWalletRepositoryTestSuite) TestTeamWalletRepository_CreateError() {
	teamWallet := mockModelTeamWallet()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "team_wallets" ("name","balance","active","daily_limit","daily_usage","monthly_limit","monthly_usage","created_at","updated_at") VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING "id"`)).
		WithArgs(teamWallet.Name, "0", nil, "0", "0", "0", "0", sqlmock.AnyArg(), sqlmock.AnyArg()).
		WillReturnError(errors.New("duplicate id"))
	t.mock.ExpectRollback()

	err := t.app.Create(context.Background(), teamWallet)
	assert.Equal(t.T(), teamWallet.ID, uint64(0))
	assert.NotNil(t.T(), err)
}

func TestTeamWalletRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(teamWalletRepositoryTestSuite))
}

func mockModelTeamWallet() *model.TeamWallet {
	return &model.TeamWallet{
		Name: "Spenmo - Procurement",
	}
}
