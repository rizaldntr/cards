package repository

import (
	"context"

	"github.com/shopspring/decimal"
	"gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gorm.io/gorm"
)

type UserCardRepository struct {
	db     *db.DB
	logger *log.Logger
}

func NewUserCardRepository(db *db.DB, logger *log.Logger) *UserCardRepository {
	return &UserCardRepository{
		db:     db,
		logger: logger,
	}
}

func (uc *UserCardRepository) Create(ctx context.Context, userCard *model.UserCard) error {
	result := uc.db.Client.WithContext(ctx).Create(userCard)

	if err := handleDBError(result); err != nil {
		return err
	}

	return nil
}

func (uc *UserCardRepository) Update(ctx context.Context, id uint64, userCard *model.UserCard) error {
	result := uc.db.Client.WithContext(ctx).Model(&model.UserCard{}).Where("id = ?", id).Updates(userCard)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}

func (uc *UserCardRepository) Read(ctx context.Context, id uint64) (*model.UserCard, error) {
	userCard := &model.UserCard{}
	result := uc.db.Client.WithContext(ctx).First(userCard, id)

	if err := handleDBError(result); err != nil {
		return nil, err
	}

	return userCard, nil
}

func (uc *UserCardRepository) Delete(ctx context.Context, id uint64) error {
	result := uc.db.Client.WithContext(ctx).Delete(&model.UserCard{}, id)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}

func (uc *UserCardRepository) Transactions(ctx context.Context, from, to uint64, amount decimal.Decimal) error {
	err := uc.db.Client.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		// Update card usage from sender
		if err := tx.First(&model.UserCard{}, from).
			Updates(map[string]interface{}{
				"daily_usage":   gorm.Expr("daily_usage + ?", amount),
				"monthly_usage": gorm.Expr("monthly_usage + ?", amount),
			}).Error; err != nil {
			return gorm.ErrInvalidTransaction
		}

		// To avoid the deadlocks we will update from the smaller id first
		fromCard, toCard, addAmount := from, to, amount
		if toCard < fromCard {
			fromCard, toCard, addAmount = to, from, amount.Neg()
		}

		// Update balance from sender
		if res := tx.Table("user_wallets").
			Where("id = (?)", tx.Select("user_wallet_id").Where("id = ?", fromCard).Table("user_cards")).
			Update("balance", gorm.Expr("balance - ?", addAmount)); res.Error != nil || res.RowsAffected == 0 {
			return gorm.ErrInvalidTransaction
		}

		// Update balance receiver
		if res := tx.Table("user_wallets").
			Where("id = (?)", tx.Select("user_wallet_id").Where("id = ?", toCard).Table("user_cards")).
			Update("balance", gorm.Expr("balance + ?", addAmount)); res.Error != nil || res.RowsAffected == 0 {
			return gorm.ErrInvalidTransaction
		}

		return nil
	})
	if err != nil {
		return errors.ErrUnprocessableEntity
	}

	return nil
}
