package repository

import (
	"github.com/lib/pq"
	"gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/pkg/response"
	"gorm.io/gorm"
)

// nolint:errorlint,cyclop
func handleDBError(result *gorm.DB) error {
	if result.Error != nil {
		switch result.Error {
		case gorm.ErrRecordNotFound:
			return errors.ErrRecordNotFound
		case gorm.ErrInvalidData:
			return errors.ErrInvalidParameters
		case gorm.ErrInvalidField:
			return errors.ErrInvalidParameters
		case gorm.ErrInvalidValueOfLength:
			return errors.ErrInvalidParameters
		case gorm.ErrInvalidValue:
			return errors.ErrInvalidParameters
		case gorm.ErrPrimaryKeyRequired:
			return errors.ErrInvalidParameters
		default:
			return handleErrorUsingPostgresErrorCode(result)
		}
	}

	return nil
}

func handleErrorUsingPostgresErrorCode(result *gorm.DB) error {
	if _, ok := result.Error.(*pq.Error); ok {
		errCode := result.Error.(*pq.Error).Code
		switch errCode {
		case errors.UniqueConstraintError:
			return errors.ErrRecordConflict
		}
	}

	return response.ErrUnexpectedError
}
