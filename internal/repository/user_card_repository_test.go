package repository_test

import (
	"context"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/repository"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type userCardRepositoryTestSuite struct {
	suite.Suite
	db     *db.DB
	mock   sqlmock.Sqlmock
	logger *log.Logger

	app *repository.UserCardRepository
}

func (t *userCardRepositoryTestSuite) SetupTest() {
	pql, mock, _ := sqlmock.New()
	gormDB, _ := gorm.Open(postgres.New(postgres.Config{Conn: pql}), &gorm.Config{})

	t.db = &db.DB{Client: gormDB}
	t.mock = mock
	t.logger = &log.Logger{}
	t.app = repository.NewUserCardRepository(t.db, t.logger)
}

func (t *userCardRepositoryTestSuite) TestNewUserCardRepository() {
	teamWalletRepository := repository.NewUserCardRepository(&db.DB{}, &log.Logger{})
	assert.NotNil(t.T(), teamWalletRepository)
}

func (t *userCardRepositoryTestSuite) TestUserCardRepository_Delete() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "user_cards" WHERE "user_cards"."id" = $1`)).
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()
	err := t.app.Delete(context.Background(), 1)
	assert.Nil(t.T(), err)
}

func (t *userCardRepositoryTestSuite) TestUserCardRepository_DeleteError() {
	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "user_cards" WHERE "user_cards"."id" = $1`)).
		WithArgs(1).
		WillReturnError(errors.New("Error"))
	t.mock.ExpectRollback()
	err := t.app.Delete(context.Background(), 1)
	assert.NotNil(t.T(), err)
}

func (t *userCardRepositoryTestSuite) TestUserCardRepository_Read() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "user_cards" WHERE "user_cards"."id" = $1 ORDER BY "user_cards"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	user, err := t.app.Read(context.Background(), 1)
	assert.Nil(t.T(), err)
	assert.NotNil(t.T(), user)
}

func (t *userCardRepositoryTestSuite) TestUserCardRepository_ReadError() {
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "user_cards" WHERE "user_cards"."id" = $1 ORDER BY "user_cards"."id" LIMIT 1`)).
		WithArgs(1).
		WillReturnError(errors.New("Not found"))
	user, err := t.app.Read(context.Background(), 1)
	assert.NotNil(t.T(), err)
	assert.Nil(t.T(), user)
}

func (t *userCardRepositoryTestSuite) TestUserCardRepository_Update() {
	userCard := mockModelUserCard()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "user_cards" SET "user_wallet_id"=$1,"name"=$2,"number"=$3,"valid"=$4,"updated_at"=$5 WHERE id = $6`)).
		WithArgs(userCard.UserWalletID, userCard.Name, userCard.Number, userCard.Valid, sqlmock.AnyArg(), 1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()

	err := t.app.Update(context.Background(), 1, userCard)
	assert.Nil(t.T(), err)
}

func (t *userCardRepositoryTestSuite) TestUserCardRepository_UpdateError() {
	userCard := mockModelUserCard()

	t.mock.ExpectBegin()
	t.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "user_cards" SET "user_wallet_id"=$1,"name"=$2,"number"=$3,"valid"=$4,"updated_at"=$5 WHERE id = $6`)).
		WithArgs(userCard.UserWalletID, userCard.Name, userCard.Number, userCard.Valid, sqlmock.AnyArg(), 1).
		WillReturnError(errors.New("Resource not found"))
	t.mock.ExpectRollback()

	err := t.app.Update(context.Background(), 1, userCard)
	assert.NotNil(t.T(), err)
	assert.Equal(t.T(), userCard.ID, uint64(0))
}

func (t *userCardRepositoryTestSuite) TestUserCardRepository_Create() {
	userCard := mockModelUserCard()
	userCard.UserWalletID = 1

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "user_cards" ("created_at","updated_at","user_wallet_id","name","number","valid") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "user_wallet_id","name","number","valid","active","daily_limit","daily_usage","monthly_limit","monthly_usage","id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), userCard.UserWalletID, userCard.Name, userCard.Number, userCard.Valid).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	t.mock.ExpectCommit()

	err := t.app.Create(context.Background(), userCard)
	assert.Nil(t.T(), err)
	assert.Equal(t.T(), userCard.ID, uint64(1))
}

func (t *userCardRepositoryTestSuite) TestUserCardRepository_CreateError() {
	userCard := mockModelUserCard()

	t.mock.ExpectBegin()
	t.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "user_cards" ("user_wallet_id","name","number","valid","active","daily_limit","daily_usage","monthly_limit","monthly_usage","created_at","updated_at") VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING "id"`)).
		WithArgs(1, userCard.Name, userCard.Number, userCard.Valid, nil, "0", "0", "0", "0", sqlmock.AnyArg(), sqlmock.AnyArg()).
		WillReturnError(errors.New("duplicate id"))
	t.mock.ExpectRollback()

	err := t.app.Create(context.Background(), userCard)
	assert.Equal(t.T(), userCard.ID, uint64(0))
	assert.NotNil(t.T(), err)
}

func TestUserCardRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(userCardRepositoryTestSuite))
}

func mockModelUserCard() *model.UserCard {
	return &model.UserCard{
		Name:         "Spenmo - Procurement",
		UserWalletID: uint64(1),
		Number:       "12345",
		Valid:        time.Now(),
	}
}
