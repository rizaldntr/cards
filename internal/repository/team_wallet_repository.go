package repository

import (
	"context"

	"gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

type TeamWalletRepository struct {
	db     *db.DB
	logger *log.Logger
}

func NewTeamWalletRepository(db *db.DB, logger *log.Logger) *TeamWalletRepository {
	return &TeamWalletRepository{
		db:     db,
		logger: logger,
	}
}

func (tw *TeamWalletRepository) Create(ctx context.Context, teamWallet *model.TeamWallet) error {
	result := tw.db.Client.WithContext(ctx).Create(teamWallet)

	if result.Error != nil {
		return response.ErrUnexpectedError
	}

	return nil
}

func (tw *TeamWalletRepository) Update(ctx context.Context, id uint64, teamWallet *model.TeamWallet) error {
	result := tw.db.Client.WithContext(ctx).Model(&model.TeamWallet{}).Where("id = ?", id).Updates(teamWallet)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}

func (tw *TeamWalletRepository) Read(ctx context.Context, id uint64) (*model.TeamWallet, error) {
	teamWallet := &model.TeamWallet{}
	result := tw.db.Client.WithContext(ctx).First(teamWallet, id)

	if err := handleDBError(result); err != nil {
		return nil, err
	}

	return teamWallet, nil
}

func (tw *TeamWalletRepository) Delete(ctx context.Context, id uint64) error {
	result := tw.db.Client.WithContext(ctx).Delete(&model.TeamWallet{}, id)

	if err := handleDBError(result); err != nil {
		return err
	}

	if result.RowsAffected == 0 {
		return errors.ErrRecordNotFound
	}

	return nil
}
