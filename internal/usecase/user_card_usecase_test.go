package usecase_test

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/usecase"
	"gitlab.com/rizaldntr/cards/mocks"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

func TestNewUserCardUsecase(t *testing.T) {
	userCardUsecase := usecase.NewUserCardUsecase(new(mocks.UserCardRepository), &log.Logger{})
	assert.NotNil(t, userCardUsecase)
}

func TestUserCardUsecase_Create(t1 *testing.T) {
	ctx := context.TODO()
	userCard := mockUserCard()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx      context.Context
		userCard *model.UserCard
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.UserCard
		wantErr  assert.ErrorAssertionFunc
	}{
		{
			name: "create user card success",
			args: args{
				ctx:      ctx,
				userCard: userCard,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, userCard},
				returnArgs: []interface{}{nil},
			},
			wantErr: assert.NoError,
			want:    userCard,
		},
		{
			name: "create team error",
			args: args{
				ctx:      ctx,
				userCard: userCard,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, userCard},
				returnArgs: []interface{}{errors.New("error when connecting to db")},
			},
			wantErr: assert.Error,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedUserCardUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Create(tt.args.ctx, tt.args.userCard)
			if !tt.wantErr(t1, err, fmt.Sprintf("Create(%v, %v)", tt.args.ctx, tt.args.userCard)) {
				return
			}
		})
	}
}

func TestUserCardUsecase_Delete(t1 *testing.T) {
	ctx := context.TODO()
	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		wantErr  bool
	}{
		{
			name: "delete team success",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil},
			},
			wantErr: false,
		},
		{
			name: "delete team error",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{errors.New("not found")},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedUserCardUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			if err := t.Delete(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t1.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserCardUsecase_Read(t1 *testing.T) {
	ctx := context.TODO()
	userCard := mockUserCard()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		mockArgs mockArgs
		args     args
		want     *model.UserCard
		wantErr  bool
	}{
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{userCard, nil},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    userCard,
			wantErr: false,
		},
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil, errors.New("record not found")},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedUserCardUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			got, err := t.Read(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("Read() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserCardUsecase_Update(t1 *testing.T) {
	ctx := context.TODO()
	userCard := mockUserCard()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx      context.Context
		id       uint64
		userCard *model.UserCard
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.UserCard
		wantErr  bool
	}{
		{
			name: "update user card success",
			args: args{
				ctx:      ctx,
				id:       1,
				userCard: userCard,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), userCard},
				returnArgs: []interface{}{nil},
			},
			want:    userCard,
			wantErr: false,
		},
		{
			name: "update user card failed",
			args: args{
				ctx:      ctx,
				id:       1,
				userCard: userCard,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), userCard},
				returnArgs: []interface{}{errors.New("not found")},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedUserCardUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Update(tt.args.ctx, tt.args.id, tt.args.userCard)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func createMockedUserCardUsecase(methodName string, inputArgs, returnArgs []interface{}) *usecase.UserCardUsecase {
	userCardRepository := new(mocks.UserCardRepository)
	userCardUsecase := usecase.NewUserCardUsecase(userCardRepository, &log.Logger{})
	userCardRepository.On(methodName, inputArgs...).Return(returnArgs...).Once()
	return userCardUsecase
}

func mockUserCard() *model.UserCard {
	return &model.UserCard{
		Name:         "Spenmo",
		UserWalletID: 1,
		Number:       "1234567890",
		Valid:        time.Now(),
		DailyUsage:   decimal.New(1000, 0),
		DailyLimit:   decimal.New(1000, 0),
	}
}
