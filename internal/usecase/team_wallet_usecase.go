package usecase

import (
	"context"

	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

type TeamWalletUsecase struct {
	twRepository model.TeamWalletRepository
	logger       *log.Logger
}

func NewTeamWalletUsecase(twRepository model.TeamWalletRepository, logger *log.Logger) *TeamWalletUsecase {
	return &TeamWalletUsecase{
		twRepository: twRepository,
		logger:       logger,
	}
}

func (tw *TeamWalletUsecase) Create(ctx context.Context, teamWallet *model.TeamWallet) error {
	return tw.twRepository.Create(ctx, teamWallet)
}

func (tw *TeamWalletUsecase) Update(ctx context.Context, id uint64, teamWallet *model.TeamWallet) error {
	return tw.twRepository.Update(ctx, id, teamWallet)
}

func (tw *TeamWalletUsecase) Read(ctx context.Context, id uint64) (*model.TeamWallet, error) {
	return tw.twRepository.Read(ctx, id)
}

func (tw *TeamWalletUsecase) Delete(ctx context.Context, id uint64) error {
	return tw.twRepository.Delete(ctx, id)
}
