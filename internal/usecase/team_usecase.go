package usecase

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

type TeamUsecase struct {
	tRepository model.TeamRepository
	logger      *log.Logger
}

func NewTeamUsecase(tRepository model.TeamRepository, logger *log.Logger) *TeamUsecase {
	return &TeamUsecase{
		tRepository: tRepository,
		logger:      logger,
	}
}

func (t *TeamUsecase) Create(ctx context.Context, team *model.Team) error {
	err := t.tRepository.Create(ctx, team)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (t *TeamUsecase) Update(ctx context.Context, id uint64, team *model.Team) error {
	err := t.tRepository.Update(ctx, id, team)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (t *TeamUsecase) Read(ctx context.Context, id uint64) (*model.Team, error) {
	team, err := t.tRepository.Read(ctx, id)
	if err != nil {
		return team, errors.WithStack(err)
	}

	return team, nil
}

func (t *TeamUsecase) Delete(ctx context.Context, id uint64) error {
	return t.tRepository.Delete(ctx, id)
}
