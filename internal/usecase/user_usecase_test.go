package usecase_test

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/usecase"
	"gitlab.com/rizaldntr/cards/mocks"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

func TestNewUserUsecase(t *testing.T) {
	userUsecase := usecase.NewUserUsecase(new(mocks.UserRepository), &log.Logger{})
	assert.NotNil(t, userUsecase)
}

func TestUserUsecase_Create(t1 *testing.T) {
	ctx := context.TODO()
	user := mockUser()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx  context.Context
		user *model.User
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.User
		wantErr  assert.ErrorAssertionFunc
	}{
		{
			name: "create user success",
			args: args{
				ctx:  ctx,
				user: user,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, user},
				returnArgs: []interface{}{nil},
			},
			wantErr: assert.NoError,
			want:    user,
		},
		{
			name: "create user error",
			args: args{
				ctx:  ctx,
				user: user,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, user},
				returnArgs: []interface{}{errors.New("error when connecting to db")},
			},
			wantErr: assert.Error,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := mockUserRepository(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Create(tt.args.ctx, tt.args.user)
			if !tt.wantErr(t1, err, fmt.Sprintf("Create(%v, %v)", tt.args.ctx, tt.args.user)) {
				return
			}
		})
	}
}

func TestUserUsecase_Delete(t1 *testing.T) {
	ctx := context.TODO()
	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		wantErr  bool
	}{
		{
			name: "delete team success",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil},
			},
			wantErr: false,
		},
		{
			name: "delete team error",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{errors.New("not found")},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := mockUserRepository(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			if err := t.Delete(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t1.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserUsecase_Read(t1 *testing.T) {
	ctx := context.TODO()
	user := mockUser()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		mockArgs mockArgs
		args     args
		want     *model.User
		wantErr  bool
	}{
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{user, nil},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    user,
			wantErr: false,
		},
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil, errors.New("record not found")},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := mockUserRepository(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			got, err := t.Read(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("Read() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserUsecase_Update(t1 *testing.T) {
	ctx := context.TODO()
	user := mockUser()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx  context.Context
		id   uint64
		user *model.User
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.User
		wantErr  bool
	}{
		{
			name: "update user success",
			args: args{
				ctx:  ctx,
				id:   1,
				user: user,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), user},
				returnArgs: []interface{}{nil},
			},
			want:    user,
			wantErr: false,
		},
		{
			name: "update user failed",
			args: args{
				ctx:  ctx,
				id:   1,
				user: user,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), user},
				returnArgs: []interface{}{errors.New("not found")},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := mockUserRepository(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Update(tt.args.ctx, tt.args.id, tt.args.user)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func mockUserRepository(methodName string, inputArgs, returnArgs []interface{}) *usecase.UserUsecase {
	userRepository := new(mocks.UserRepository)
	userUsecase := usecase.NewUserUsecase(userRepository, &log.Logger{})
	userRepository.On(methodName, inputArgs...).Return(returnArgs...).Once()
	return userUsecase
}

func mockUser() *model.User {
	return &model.User{
		Name:     "Josh",
		Email:    "josh@gmail.com",
		Username: "josh",
		Password: "reallyStrongRight?",
	}
}
