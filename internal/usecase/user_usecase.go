package usecase

import (
	"context"

	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

type UserUsecase struct {
	uRepository model.UserRepository
	logger      *log.Logger
}

func NewUserUsecase(uRepository model.UserRepository, logger *log.Logger) *UserUsecase {
	return &UserUsecase{
		uRepository: uRepository,
		logger:      logger,
	}
}

func (u *UserUsecase) Create(ctx context.Context, user *model.User) error {
	return u.uRepository.Create(ctx, user)
}

func (u *UserUsecase) Update(ctx context.Context, id uint64, user *model.User) error {
	return u.uRepository.Update(ctx, id, user)
}

func (u *UserUsecase) Read(ctx context.Context, id uint64) (*model.User, error) {
	return u.uRepository.Read(ctx, id)
}

func (u *UserUsecase) Delete(ctx context.Context, id uint64) error {
	return u.uRepository.Delete(ctx, id)
}
