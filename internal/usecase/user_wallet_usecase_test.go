package usecase_test

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/usecase"
	"gitlab.com/rizaldntr/cards/mocks"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

func TestNewUserWalletUsecase(t *testing.T) {
	userWalletUsecase := usecase.NewUserWalletUsecase(new(mocks.UserWalletUsecase), &log.Logger{})
	assert.NotNil(t, userWalletUsecase)
}

func TestUserWalletUsecase_Create(t1 *testing.T) {
	ctx := context.TODO()
	userWallet := mockUserWallet()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx        context.Context
		userWallet *model.UserWallet
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.UserWallet
		wantErr  assert.ErrorAssertionFunc
	}{
		{
			name: "create user wallet success",
			args: args{
				ctx:        ctx,
				userWallet: userWallet,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, userWallet},
				returnArgs: []interface{}{nil},
			},
			wantErr: assert.NoError,
			want:    userWallet,
		},
		{
			name: "create user wallet error",
			args: args{
				ctx:        ctx,
				userWallet: userWallet,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, userWallet},
				returnArgs: []interface{}{errors.New("error when connecting to db")},
			},
			wantErr: assert.Error,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := mockUserWalletRepository(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Create(tt.args.ctx, tt.args.userWallet)
			if !tt.wantErr(t1, err, fmt.Sprintf("Create(%v, %v)", tt.args.ctx, tt.args.userWallet)) {
				return
			}
		})
	}
}

func TestUserWalletUsecase_Delete(t1 *testing.T) {
	ctx := context.TODO()
	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		wantErr  bool
	}{
		{
			name: "delete team success",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil},
			},
			wantErr: false,
		},
		{
			name: "delete team error",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{errors.New("not found")},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := mockUserWalletRepository(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			if err := t.Delete(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t1.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserWalletUsecase_Read(t1 *testing.T) {
	ctx := context.TODO()
	userWallet := mockUserWallet()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		mockArgs mockArgs
		args     args
		want     *model.UserWallet
		wantErr  bool
	}{
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{userWallet, nil},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    userWallet,
			wantErr: false,
		},
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil, errors.New("record not found")},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := mockUserWalletRepository(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			got, err := t.Read(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("Read() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserWalletUsecase_Update(t1 *testing.T) {
	ctx := context.TODO()
	userWallet := mockUserWallet()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx        context.Context
		id         uint64
		userWallet *model.UserWallet
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.UserWallet
		wantErr  bool
	}{
		{
			name: "update user wallet success",
			args: args{
				ctx:        ctx,
				id:         1,
				userWallet: userWallet,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), userWallet},
				returnArgs: []interface{}{nil},
			},
			want:    userWallet,
			wantErr: false,
		},
		{
			name: "update user wallet failed",
			args: args{
				ctx:        ctx,
				id:         1,
				userWallet: userWallet,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), userWallet},
				returnArgs: []interface{}{errors.New("not found")},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := mockUserWalletRepository(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Update(tt.args.ctx, tt.args.id, tt.args.userWallet)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func mockUserWalletRepository(methodName string, inputArgs, returnArgs []interface{}) *usecase.UserWalletUsecase {
	userWalletRepository := new(mocks.UserWalletRepository)
	userWalletUsecase := usecase.NewUserWalletUsecase(userWalletRepository, &log.Logger{})
	userWalletRepository.On(methodName, inputArgs...).Return(returnArgs...).Once()
	return userWalletUsecase
}

func mockUserWallet() *model.UserWallet {
	return &model.UserWallet{
		Name:    "Primary Wallet",
		UserID:  1,
		Balance: decimal.New(100000000, 0),
	}
}
