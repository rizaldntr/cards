package usecase_test

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/usecase"
	"gitlab.com/rizaldntr/cards/mocks"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

func TestNewTeamWalletUsecase(t *testing.T) {
	teamWalletUsecase := usecase.NewTeamWalletUsecase(new(mocks.TeamWalletRepository), &log.Logger{})
	assert.NotNil(t, teamWalletUsecase)
}

func TestTeamWalletUsecase_Create(t1 *testing.T) {
	ctx := context.TODO()
	teamWallet := mockTeamWallet()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx        context.Context
		teamWallet *model.TeamWallet
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.TeamWallet
		wantErr  assert.ErrorAssertionFunc
	}{
		{
			name: "create team wallet success",
			args: args{
				ctx:        ctx,
				teamWallet: teamWallet,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, teamWallet},
				returnArgs: []interface{}{nil},
			},
			wantErr: assert.NoError,
			want:    teamWallet,
		},
		{
			name: "create team error",
			args: args{
				ctx:        ctx,
				teamWallet: teamWallet,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, teamWallet},
				returnArgs: []interface{}{errors.New("error when connecting to db")},
			},
			wantErr: assert.Error,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedTeamWalletUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Create(tt.args.ctx, tt.args.teamWallet)
			if !tt.wantErr(t1, err, fmt.Sprintf("Create(%v, %v)", tt.args.ctx, tt.args.teamWallet)) {
				return
			}
		})
	}
}

func TestTeamWalletUsecase_Delete(t1 *testing.T) {
	ctx := context.TODO()
	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		wantErr  bool
	}{
		{
			name: "delete team success",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil},
			},
			wantErr: false,
		},
		{
			name: "delete team error",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{errors.New("not found")},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedTeamWalletUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			if err := t.Delete(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t1.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTeamWalletUsecase_Read(t1 *testing.T) {
	ctx := context.TODO()
	teamWallet := mockTeamWallet()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		mockArgs mockArgs
		args     args
		want     *model.TeamWallet
		wantErr  bool
	}{
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{teamWallet, nil},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    teamWallet,
			wantErr: false,
		},
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil, errors.New("record not found")},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedTeamWalletUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			got, err := t.Read(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("Read() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTeamWalletUsecase_Update(t1 *testing.T) {
	ctx := context.TODO()
	teamWallet := mockTeamWallet()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx        context.Context
		id         uint64
		teamWallet *model.TeamWallet
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.TeamWallet
		wantErr  bool
	}{
		{
			name: "update team wallet success",
			args: args{
				ctx:        ctx,
				id:         1,
				teamWallet: teamWallet,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), teamWallet},
				returnArgs: []interface{}{nil},
			},
			want:    teamWallet,
			wantErr: false,
		},
		{
			name: "update team failed",
			args: args{
				ctx:        ctx,
				id:         1,
				teamWallet: teamWallet,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), teamWallet},
				returnArgs: []interface{}{errors.New("not found")},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedTeamWalletUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Update(tt.args.ctx, tt.args.id, tt.args.teamWallet)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func createMockedTeamWalletUsecase(methodName string, inputArgs, returnArgs []interface{}) *usecase.TeamWalletUsecase {
	teamWalletRepository := new(mocks.TeamWalletRepository)
	teamWalletUsecase := usecase.NewTeamWalletUsecase(teamWalletRepository, &log.Logger{})
	teamWalletRepository.On(methodName, inputArgs...).Return(returnArgs...).Once()
	return teamWalletUsecase
}

func mockTeamWallet() *model.TeamWallet {
	return &model.TeamWallet{
		Name:    "Spenmo",
		Balance: decimal.New(1000000, 0),
	}
}
