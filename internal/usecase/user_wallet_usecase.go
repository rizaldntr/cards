package usecase

import (
	"context"

	"github.com/shopspring/decimal"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

type UserWalletUsecase struct {
	uwRepository model.UserWalletRepository
	logger       *log.Logger
}

func NewUserWalletUsecase(uwRepository model.UserWalletRepository, logger *log.Logger) *UserWalletUsecase {
	return &UserWalletUsecase{
		uwRepository: uwRepository,
		logger:       logger,
	}
}

func (w *UserWalletUsecase) Create(ctx context.Context, userWallet *model.UserWallet) error {
	return w.uwRepository.Create(ctx, userWallet)
}

func (w *UserWalletUsecase) Update(ctx context.Context, id uint64, userWallet *model.UserWallet) error {
	return w.uwRepository.Update(ctx, id, userWallet)
}

func (w *UserWalletUsecase) Read(ctx context.Context, id uint64) (*model.UserWallet, error) {
	return w.uwRepository.Read(ctx, id)
}

func (w *UserWalletUsecase) Delete(ctx context.Context, id uint64) error {
	return w.uwRepository.Delete(ctx, id)
}

func (w *UserWalletUsecase) Topup(ctx context.Context, id uint64, amount decimal.Decimal) error {
	return w.uwRepository.Topup(ctx, id, amount)
}
