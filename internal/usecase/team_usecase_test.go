package usecase_test

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/usecase"
	"gitlab.com/rizaldntr/cards/mocks"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

func TestNewTeamUsecase(t *testing.T) {
	teamUsecase := usecase.NewTeamUsecase(new(mocks.TeamRepository), &log.Logger{})
	assert.NotNil(t, teamUsecase)
}

func TestTeamUsecase_Create(t1 *testing.T) {
	ctx := context.TODO()
	team := mockTeam()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx  context.Context
		team *model.Team
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.Team
		wantErr  assert.ErrorAssertionFunc
	}{
		{
			name: "create team success",
			args: args{
				ctx:  ctx,
				team: team,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, team},
				returnArgs: []interface{}{nil},
			},
			wantErr: assert.NoError,
			want:    team,
		},
		{
			name: "create team error",
			args: args{
				ctx:  ctx,
				team: team,
			},
			mockArgs: mockArgs{
				methodName: "Create",
				inputArgs:  []interface{}{ctx, team},
				returnArgs: []interface{}{errors.New("error when connecting to db")},
			},
			wantErr: assert.Error,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedTeamUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Create(tt.args.ctx, tt.args.team)
			if !tt.wantErr(t1, err, fmt.Sprintf("Create(%v, %v)", tt.args.ctx, tt.args.team)) {
				return
			}
		})
	}
}

func TestTeamUsecase_Delete(t1 *testing.T) {
	ctx := context.TODO()
	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		wantErr  bool
	}{
		{
			name: "delete team success",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil},
			},
			wantErr: false,
		},
		{
			name: "delete team error",
			args: args{
				ctx: ctx,
				id:  1,
			},
			mockArgs: mockArgs{
				methodName: "Delete",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{errors.New("not found")},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			teamRepository := new(mocks.TeamRepository)
			t := createMockedTeamUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)

			if err := t.Delete(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t1.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}

			teamRepository.AssertExpectations(t1)
		})
	}
}

func TestTeamUsecase_Read(t1 *testing.T) {
	ctx := context.TODO()
	team := mockTeam()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name     string
		mockArgs mockArgs
		args     args
		want     *model.Team
		wantErr  bool
	}{
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{team, nil},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    team,
			wantErr: false,
		},
		{
			name: "get team success",
			mockArgs: mockArgs{
				methodName: "Read",
				inputArgs:  []interface{}{ctx, uint64(1)},
				returnArgs: []interface{}{nil, errors.New("record not found")},
			},
			args: args{
				ctx: ctx,
				id:  1,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedTeamUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			got, err := t.Read(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("Read() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTeamUsecase_Update(t1 *testing.T) {
	ctx := context.TODO()
	team := mockTeam()

	type mockArgs struct {
		methodName string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	type args struct {
		ctx  context.Context
		id   uint64
		team *model.Team
	}
	tests := []struct {
		name     string
		args     args
		mockArgs mockArgs
		want     *model.Team
		wantErr  bool
	}{
		{
			name: "update team success",
			args: args{
				ctx:  ctx,
				id:   1,
				team: team,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), team},
				returnArgs: []interface{}{nil},
			},
			want:    team,
			wantErr: false,
		},
		{
			name: "update team failed",
			args: args{
				ctx:  ctx,
				id:   1,
				team: team,
			},
			mockArgs: mockArgs{
				methodName: "Update",
				inputArgs:  []interface{}{ctx, uint64(1), team},
				returnArgs: []interface{}{errors.New("not found")},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := createMockedTeamUsecase(tt.mockArgs.methodName, tt.mockArgs.inputArgs, tt.mockArgs.returnArgs)
			err := t.Update(tt.args.ctx, tt.args.id, tt.args.team)
			if (err != nil) != tt.wantErr {
				t1.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func createMockedTeamUsecase(methodName string, inputArgs, returnArgs []interface{}) *usecase.TeamUsecase {
	teamRepository := new(mocks.TeamRepository)
	teamUsecase := usecase.NewTeamUsecase(teamRepository, &log.Logger{})
	teamRepository.On(methodName, inputArgs...).Return(returnArgs...).Once()
	return teamUsecase
}

func mockTeam() *model.Team {
	return &model.Team{
		Name: "Spenmo",
	}
}
