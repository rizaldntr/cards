package usecase

import (
	"context"

	"github.com/shopspring/decimal"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

type UserCardUsecase struct {
	ucRepository model.UserCardRepository
	logger       *log.Logger
}

func NewUserCardUsecase(ucRepository model.UserCardRepository, logger *log.Logger) *UserCardUsecase {
	return &UserCardUsecase{
		ucRepository: ucRepository,
		logger:       logger,
	}
}

func (w *UserCardUsecase) Create(ctx context.Context, userCard *model.UserCard) error {
	return w.ucRepository.Create(ctx, userCard)
}

func (w *UserCardUsecase) Update(ctx context.Context, id uint64, userCard *model.UserCard) error {
	return w.ucRepository.Update(ctx, id, userCard)
}

func (w *UserCardUsecase) Read(ctx context.Context, id uint64) (*model.UserCard, error) {
	return w.ucRepository.Read(ctx, id)
}

func (w *UserCardUsecase) Delete(ctx context.Context, id uint64) error {
	return w.ucRepository.Delete(ctx, id)
}

func (w *UserCardUsecase) Transactions(ctx context.Context, from, to uint64, amount decimal.Decimal) error {
	return w.ucRepository.Transactions(ctx, from, to, amount)
}
