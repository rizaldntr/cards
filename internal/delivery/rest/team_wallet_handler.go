package rest

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	appErr "gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/middleware"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

type TeamWalletHandler struct {
	teamWalletUsecase model.TeamWalletUsecase
	validator         *validator.Validate
}

type TeamWalletCreateRequest struct {
	Name   string `json:"name" validate:"required"`
	TeamID uint64 `json:"teamID" validate:"required"`
}

type TeamWalletUpdateRequest struct {
	Name         string          `json:"name,omitempty"`
	Active       sql.NullBool    `json:"active"`
	DailyLimit   decimal.Decimal `json:"dailyLimit"`
	MonthlyLimit decimal.Decimal `json:"monthlyLimit"`
}

func NewTeamWalletHandler(TeamWalletUsecase model.TeamWalletUsecase, validator *validator.Validate) *TeamWalletHandler {
	return &TeamWalletHandler{
		teamWalletUsecase: TeamWalletUsecase,
		validator:         validator,
	}
}

func (tw *TeamWalletHandler) Register(router *httprouter.Router, middlewares ...middleware.Middleware) {
	router.GET("/team-wallets/:id", middleware.UseMiddleware(tw.Get, middlewares...))
	router.POST("/team-wallets", middleware.UseMiddleware(tw.Create, middlewares...))
	router.PATCH("/team-wallets/:id", middleware.UseMiddleware(tw.Update, middlewares...))
	router.DELETE("/team-wallets/:id", middleware.UseMiddleware(tw.Delete, middlewares...))
}

func (tw *TeamWalletHandler) Get(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	teamWallet, err := tw.teamWalletUsecase.Read(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	response.OK(w, teamWallet, "Success")

	return nil
}

func (tw *TeamWalletHandler) Create(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := TeamWalletCreateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	if err := ValidateRequiredParameters(tw.validator, body); len(err) != 0 {
		response.Error(w, err...)

		return errors.WithStack(err[0])
	}

	teamWallet := model.TeamWallet{
		TeamID: body.TeamID,
		Name:   body.Name,
	}

	err := tw.teamWalletUsecase.Create(r.Context(), &teamWallet)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	response.Created(w, teamWallet)

	return nil
}

func (tw *TeamWalletHandler) Update(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := TeamWalletUpdateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	teamWallet := model.TeamWallet{
		Name:         body.Name,
		Active:       body.Active,
		DailyLimit:   body.DailyLimit,
		MonthlyLimit: body.MonthlyLimit,
	}

	err = tw.teamWalletUsecase.Update(r.Context(), id, &teamWallet)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}

func (tw *TeamWalletHandler) Delete(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	err = tw.teamWalletUsecase.Delete(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}
