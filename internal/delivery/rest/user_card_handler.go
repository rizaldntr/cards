package rest

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"gitlab.com/rizaldntr/cards/internal/constant"
	appErr "gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/middleware"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

type UserCardHandler struct {
	userCardUsecase model.UserCardUsecase
	validator       *validator.Validate
}

type UserCardCreateRequest struct {
	Name         string `json:"name" validate:"required"`
	UserWalletID uint64 `json:"userWalletId" validate:"required"`
}

type UserCardUpdateRequest struct {
	Name         string          `json:"name,omitempty"`
	Valid        time.Time       `json:"valid"`
	Active       bool            `json:"active"`
	DailyLimit   decimal.Decimal `json:"dailyLimit"`
	MonthlyLimit decimal.Decimal `json:"monthlyLimit"`
}

type UserCardTransactionsRequest struct {
	From   uint64          `json:"from" validate:"required"`
	To     uint64          `json:"to" validate:"required"`
	Amount decimal.Decimal `json:"amount" validate:"required"`
}

func NewUserCardHandler(userCardUsecase model.UserCardUsecase, validator *validator.Validate) *UserCardHandler {
	return &UserCardHandler{
		userCardUsecase: userCardUsecase,
		validator:       validator,
	}
}

func (uc *UserCardHandler) Register(router *httprouter.Router, middlewares ...middleware.Middleware) {
	router.GET("/cards/:id", middleware.UseMiddleware(uc.Get, middlewares...))
	router.POST("/cards", middleware.UseMiddleware(uc.Create, middlewares...))
	router.PATCH("/cards/:id", middleware.UseMiddleware(uc.Update, middlewares...))
	router.DELETE("/cards/:id", middleware.UseMiddleware(uc.Delete, middlewares...))
	router.POST("/card-transactions", middleware.UseMiddleware(uc.Transactions, middlewares...))
}

func (uc *UserCardHandler) Get(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	userCard, err := uc.userCardUsecase.Read(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	response.OK(w, userCard, "Success")

	return nil
}

func (uc *UserCardHandler) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	body := UserCardCreateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	if err := ValidateRequiredParameters(uc.validator, body); len(err) != 0 {
		response.Error(w, err...)

		return errors.WithStack(err[0])
	}

	card := model.UserCard{
		Name:         body.Name,
		UserWalletID: body.UserWalletID,
		Number:       uc.generateNumber(),
		Valid:        time.Now().AddDate(5, 0, 0),
		Active:       true,
		DailyLimit:   decimal.New(constant.DefaultDailyLimit, 0),
		DailyUsage:   decimal.New(0, 0),
		MonthlyLimit: decimal.New(constant.DefaultMonthlyLimit, 0),
		MonthlyUsage: decimal.New(0, 0),
	}

	err := uc.userCardUsecase.Create(r.Context(), &card)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	response.Created(w, card)

	return nil
}

func (uc *UserCardHandler) Update(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := UserCardUpdateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	card := model.UserCard{
		Name:         body.Name,
		Valid:        body.Valid,
		Active:       body.Active,
		DailyLimit:   body.DailyLimit,
		MonthlyLimit: body.MonthlyLimit,
	}

	err = uc.userCardUsecase.Update(r.Context(), id, &card)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}

func (uc *UserCardHandler) Delete(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	err = uc.userCardUsecase.Delete(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}

func (uc *UserCardHandler) Transactions(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := UserCardTransactionsRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	if err := ValidateRequiredParameters(uc.validator, body); len(err) != 0 {
		response.Error(w, err...)

		return errors.WithStack(err[0])
	}

	err := uc.userCardUsecase.Transactions(r.Context(), body.From, body.To, body.Amount)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}

func (uc *UserCardHandler) generateNumber() string {
	return fmt.Sprintf("%012d", rand.Int63n(1e16))
}
