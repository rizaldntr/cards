package rest

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	appErr "gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/middleware"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

type UserWalletHandler struct {
	userWalletUsecase model.UserWalletUsecase
	validator         *validator.Validate
}

type UserWalletCreateRequest struct {
	Name   string `json:"name" validate:"required"`
	UserID uint64 `json:"userId" validate:"required"`
}

type UserWalletUpdateRequest struct {
	Name   string `json:"name,omitempty"`
	Active bool   `json:"active"`
}

type UserWalletTopupRequest struct {
	ID     uint64          `json:"id" validate:"required"`
	Amount decimal.Decimal `json:"amount" validate:"required"`
}

func NewUserWalletHandler(userWalletUsecase model.UserWalletUsecase, validator *validator.Validate) *UserWalletHandler {
	return &UserWalletHandler{
		userWalletUsecase: userWalletUsecase,
		validator:         validator,
	}
}

func (uw *UserWalletHandler) Register(router *httprouter.Router, middlewares ...middleware.Middleware) {
	router.GET("/wallets/:id", middleware.UseMiddleware(uw.Get, middlewares...))
	router.POST("/wallets", middleware.UseMiddleware(uw.Create, middlewares...))
	router.PATCH("/wallets/:id", middleware.UseMiddleware(uw.Update, middlewares...))
	router.DELETE("/wallets/:id", middleware.UseMiddleware(uw.Delete, middlewares...))
	router.POST("/topup-wallets", middleware.UseMiddleware(uw.Topup, middlewares...))
}

func (uw *UserWalletHandler) Get(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	userWallet, err := uw.userWalletUsecase.Read(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	response.OK(w, userWallet, "Success")

	return nil
}

func (uw *UserWalletHandler) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	body := &UserWalletCreateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	if err := ValidateRequiredParameters(uw.validator, body); len(err) != 0 {
		response.Error(w, err...)

		return errors.WithStack(err[0])
	}

	userWallet := model.UserWallet{
		UserID:  body.UserID,
		Name:    body.Name,
		Balance: decimal.New(0, 0),
		Active:  true,
	}

	err := uw.userWalletUsecase.Create(r.Context(), &userWallet)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	response.Created(w, userWallet)

	return nil
}

func (uw *UserWalletHandler) Update(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := UserWalletUpdateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	userWallet := model.UserWallet{
		Name:   body.Name,
		Active: body.Active,
	}

	err = uw.userWalletUsecase.Update(r.Context(), id, &userWallet)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}

func (uw *UserWalletHandler) Delete(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	err = uw.userWalletUsecase.Delete(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}

func (uw *UserWalletHandler) Topup(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := UserWalletTopupRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	err := uw.userWalletUsecase.Topup(r.Context(), body.ID, body.Amount)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}
