package rest

import (
	"net/http"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/rizaldntr/cards/internal/application"
	"gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/pkg/instrument"
	"gitlab.com/rizaldntr/cards/pkg/middleware"
	"gitlab.com/rizaldntr/cards/pkg/response"
	"gitlab.com/rizaldntr/cards/pkg/router"
	"golang.org/x/time/rate"
)

func Get(app *application.Application) *httprouter.Router {
	newRouter := router.NewRouter()
	stdMiddlewares := getMiddleware(app)
	validator := validator.New()
	userHandler := NewUserHandler(app.Usecases.UserUsecase, validator)
	newRouter.WithHandler(userHandler, stdMiddlewares...)

	userWalletHandler := NewUserWalletHandler(app.Usecases.UserWalletUsecase, validator)
	newRouter.WithHandler(userWalletHandler, stdMiddlewares...)

	userCardHandler := NewUserCardHandler(app.Usecases.UserCardUsecase, validator)
	newRouter.WithHandler(userCardHandler, stdMiddlewares...)

	teamRouter := NewTeamHandler(app.Usecases.TeamUsecase, validator)
	newRouter.WithHandler(teamRouter, stdMiddlewares...)

	teamWalletRouter := NewTeamWalletHandler(app.Usecases.TeamWalletUsecase, validator)
	newRouter.WithHandler(teamWalletRouter, stdMiddlewares...)

	router := newRouter.Get()

	router.NotFound = http.HandlerFunc(NotFound)
	router.HandlerFunc("GET", "/metrics", promhttp.Handler().ServeHTTP)
	router.GlobalOPTIONS = http.HandlerFunc(Options)
	return router
}

func getMiddleware(app *application.Application) []middleware.Middleware {
	return []middleware.Middleware{
		middleware.WithTimeout(3 * time.Second),
		middleware.WithHTTPStatusInstrumentation(instrument.NewHistogram()),
		middleware.WithLogging(app.Logger, &middleware.NoopContextResolver{}),
		middleware.WithRateLimit(rate.Every(1*time.Second), 10000),
		middleware.WithStandardContext(),
		middleware.WithTracing(opentracing.GlobalTracer()),
	}
}

func Options(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Access-Control-Request-Method") != "" {
		// Set CORS headers
		header := w.Header()
		header.Set("Access-Control-Allow-Methods", header.Get("Allow"))
		header.Set("Access-Control-Allow-Origin", "*")
	}

	// Adjust status code to 204
	w.WriteHeader(http.StatusNoContent)
}

// NotFound for unknown path.
func NotFound(w http.ResponseWriter, _ *http.Request) {
	response.Error(w, errors.ErrPathNotFound)
}

// ValidateRequiredParameters validator runner for handler
func ValidateRequiredParameters(validate *validator.Validate, params interface{}) []error {
	var results []error
	err := validate.Struct(params)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return nil
		}

		var buildError response.CustomError
		for _, err := range err.(validator.ValidationErrors) {
			buildError = errors.ErrMissingRequiredParameters
			buildError.Field = strings.ToLower(err.Field())
			results = append(results, buildError)
		}
	}

	return results
}
