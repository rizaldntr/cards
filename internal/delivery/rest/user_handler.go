package rest

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	appErr "gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/middleware"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

type UserHandler struct {
	userUsecase model.UserUsecase
	validator   *validator.Validate
}

type UserCreateRequest struct {
	Name     string `json:"name,omitempty" validate:"required"`
	Username string `json:"username,omitempty" validate:"required"`
	Password string `json:"password" validate:"required"`
	Email    string `json:"email,omitempty" validate:"required"`
	Phone    string `json:"phone" validate:"required"`
}

type UserUpdateRequest struct {
	Name     string `json:"name,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"-"`
	Phone    string `json:"phone"`
	Email    string `json:"email,omitempty"`
	Active   bool   `json:"active"`
}

func NewUserHandler(userUsecase model.UserUsecase, validator *validator.Validate) *UserHandler {
	return &UserHandler{
		userUsecase: userUsecase,
		validator:   validator,
	}
}

func (u *UserHandler) Register(router *httprouter.Router, middlewares ...middleware.Middleware) {
	router.GET("/users/:id", middleware.UseMiddleware(u.Get, middlewares...))
	router.POST("/users", middleware.UseMiddleware(u.Create, middlewares...))
	router.PATCH("/users/:id", middleware.UseMiddleware(u.Update, middlewares...))
	router.DELETE("/users/:id", middleware.UseMiddleware(u.Delete, middlewares...))
}

func (u *UserHandler) Get(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	user, err := u.userUsecase.Read(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.Wrap(err, err.Error())
	}

	response.OK(w, user, "Success")

	return nil
}

func (u *UserHandler) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) error {
	body := UserCreateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	if err := ValidateRequiredParameters(u.validator, body); len(err) != 0 {
		response.Error(w, err...)

		return errors.WithStack(err[0])
	}

	user := model.User{
		Name:     body.Name,
		Email:    body.Email,
		Password: body.Email,
		Username: body.Username,
		Phone:    body.Phone,
		Active:   true,
	}

	err := u.userUsecase.Create(r.Context(), &user)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	response.Created(w, user)

	return nil
}

func (u *UserHandler) Update(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := UserUpdateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	user := model.User{
		Name:     body.Name,
		Username: body.Username,
		Password: body.Password,
		Phone:    body.Phone,
		Email:    body.Email,
		Active:   body.Active,
	}

	err = u.userUsecase.Update(r.Context(), id, &user)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}

func (u *UserHandler) Delete(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	err = u.userUsecase.Delete(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}
