package rest_test

import (
	"net/http"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rizaldntr/cards/internal/delivery/rest"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/mocks"
)

func TestNewTeamWalletHandler(t *testing.T) {
	teamHandler := rest.NewTeamWalletHandler(new(mocks.TeamWalletUsecase), validator.New())
	assert.NotNil(t, teamHandler)
}

func TestTeamWalletHandler_Create(t1 *testing.T) {
	type mockArgs struct {
		method     string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	tests := []struct {
		name          string
		teamCreateReq interface{}
		mockArgs      mockArgs
		status        int
	}{
		{
			name:          "no request body should return bad request",
			teamCreateReq: []byte(""),
			mockArgs:      mockArgs{},
			status:        http.StatusBadRequest,
		},
		{
			name:          "missing required parameters should return unprocessable entity",
			teamCreateReq: rest.TeamWalletCreateRequest{},
			mockArgs:      mockArgs{},
			status:        http.StatusUnprocessableEntity,
		},
		{
			name:          "team wallet usecase return unexpected error should return internal server error",
			teamCreateReq: rest.TeamWalletCreateRequest{Name: "Spenmo", TeamID: uint64(1)},
			mockArgs: mockArgs{
				method:     "Create",
				inputArgs:  []interface{}{mock.Anything, mock.Anything},
				returnArgs: []interface{}{errors.New("aaaa")},
			},
			status: http.StatusInternalServerError,
		},
		{
			name:          "team wallet usecase return nil should return success",
			teamCreateReq: rest.TeamWalletCreateRequest{Name: "Spenmo", TeamID: uint64(1)},
			mockArgs: mockArgs{
				method:     "Create",
				inputArgs:  []interface{}{mock.Anything, mock.Anything},
				returnArgs: []interface{}{nil},
			},
			status: http.StatusCreated,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			teamWalletUsecase := new(mocks.TeamWalletUsecase)
			teamWalletUsecase.On(tt.mockArgs.method, tt.mockArgs.inputArgs...).Return(tt.mockArgs.returnArgs...).Once()

			router := httprouter.New()
			handler := rest.NewTeamWalletHandler(teamWalletUsecase, validator.New())
			handler.Register(router)

			rr := request("POST", "/team-wallets", tt.teamCreateReq, router)
			assert.Equalf(t1, tt.status, rr.Code, "return = %v, want %v", rr.Code, tt.status)
		})
	}
}

func TestTeamWalletHandler_Get(t *testing.T) {
	type mockArgs struct {
		method     string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	tests := []struct {
		name     string
		mockArgs mockArgs
		status   int
	}{
		{
			name: "usecase return unexpected error should return internal server error",
			mockArgs: mockArgs{
				method:     "Read",
				inputArgs:  []interface{}{mock.Anything, uint64(1)},
				returnArgs: []interface{}{nil, errors.New("aaaa")},
			},
			status: http.StatusInternalServerError,
		},
		{
			name: "usecase return nil should return success",
			mockArgs: mockArgs{
				method:     "Read",
				inputArgs:  []interface{}{mock.Anything, uint64(1)},
				returnArgs: []interface{}{&model.TeamWallet{}, nil},
			},
			status: http.StatusOK,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t1 *testing.T) {
			teamWalletUsecase := new(mocks.TeamWalletUsecase)
			teamWalletUsecase.On(tt.mockArgs.method, tt.mockArgs.inputArgs...).Return(tt.mockArgs.returnArgs...).Once()

			router := httprouter.New()
			handler := rest.NewTeamWalletHandler(teamWalletUsecase, validator.New())
			handler.Register(router)

			rr := request("GET", "/team-wallets/1", nil, router)
			assert.Equalf(t1, tt.status, rr.Code, "return = %v, want %v", rr.Code, tt.status)
		})
	}
}
