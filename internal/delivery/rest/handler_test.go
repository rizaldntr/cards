package rest_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rizaldntr/cards/internal/application"
	"gitlab.com/rizaldntr/cards/internal/config"
	"gitlab.com/rizaldntr/cards/internal/delivery/rest"
	appError "gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/mocks"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
)

func mockApplicationConfig() *application.Application {
	userUsecase := new(mocks.UserUsecase)
	userWalletUsecase := new(mocks.UserWalletUsecase)
	userCardUsecase := new(mocks.UserCardUsecase)
	teamUsecase := new(mocks.TeamUsecase)
	teamWalletUsecase := new(mocks.TeamWalletUsecase)

	usecases := &application.Usecase{
		UserUsecase:       userUsecase,
		UserWalletUsecase: userWalletUsecase,
		UserCardUsecase:   userCardUsecase,
		TeamUsecase:       teamUsecase,
		TeamWalletUsecase: teamWalletUsecase,
	}

	return &application.Application{
		Usecases: usecases,
		Logger:   &log.Logger{},
		DB:       &db.DB{},
		Cfg:      &config.Config{},
	}
}

func TestGet(t *testing.T) {
	router := rest.Get(mockApplicationConfig())
	assert.NotNil(t, router)
}

func Test_getMiddleware(t *testing.T) {
	middlewares := rest.GetMiddleware(mockApplicationConfig())
	assert.Equal(t, len(middlewares), 6)
}

func TestValidateRequiredParameters(t *testing.T) {
	err := appError.ErrMissingRequiredParameters
	err.Field = "fieldrequired"

	type TestStruct struct {
		FieldRequired    string `validate:"required"`
		FieldNonRequired string
	}
	type args struct {
		validate *validator.Validate
		params   interface{}
	}
	tests := []struct {
		name string
		args args
		want []error
	}{
		{
			name: "return nil if all required fields are present",
			args: args{
				validate: validator.New(),
				params: TestStruct{
					FieldRequired: "present",
				},
			},
			want: []error(nil),
		},
		{
			name: "return error if any required fields are missing",
			args: args{
				validate: validator.New(),
				params: TestStruct{
					FieldNonRequired: "present",
				},
			},
			want: []error{err},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, rest.ValidateRequiredParameters(tt.args.validate, tt.args.params), "ValidateRequiredParameters(%v, %v)", tt.args.validate, tt.args.params)
		})
	}
}

func request(method, path string, body interface{}, router *httprouter.Router) *httptest.ResponseRecorder {
	var req *http.Request
	if body == nil {
		req, _ = http.NewRequest(method, path, nil)
	} else {
		reqBody, _ := json.Marshal(body)
		req, _ = http.NewRequest(method, path, bytes.NewBuffer(reqBody))
	}

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	return rr
}
