package rest_test

import (
	"net/http"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rizaldntr/cards/internal/delivery/rest"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/mocks"
)

func TestNewUserHandler(t *testing.T) {
	userHandler := rest.NewUserHandler(new(mocks.UserUsecase), validator.New())
	assert.NotNil(t, userHandler)
}

func TestUserHandler_Create(t1 *testing.T) {
	type mockArgs struct {
		method     string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	tests := []struct {
		name          string
		userCreateReq interface{}
		mockArgs      mockArgs
		status        int
	}{
		{
			name:          "no request body should return bad request",
			userCreateReq: []byte(""),
			mockArgs:      mockArgs{},
			status:        http.StatusBadRequest,
		},
		{
			name:          "missing required parameters should return unprocessable entity",
			userCreateReq: rest.UserCreateRequest{},
			mockArgs:      mockArgs{},
			status:        http.StatusUnprocessableEntity,
		},
		{
			name:          "user usecase return unexpected error should return internal server error",
			userCreateReq: rest.UserCreateRequest{Name: "Spenmo", Username: "username", Email: "email", Phone: "083734", Password: "passWorD"},
			mockArgs: mockArgs{
				method:     "Create",
				inputArgs:  []interface{}{mock.Anything, mock.Anything},
				returnArgs: []interface{}{errors.New("aaaa")},
			},
			status: http.StatusInternalServerError,
		},
		{
			name:          "user usecase return nil should return success",
			userCreateReq: rest.UserCreateRequest{Name: "Spenmo", Username: "username", Email: "email", Phone: "083734", Password: "passWorD"},
			mockArgs: mockArgs{
				method:     "Create",
				inputArgs:  []interface{}{mock.Anything, mock.Anything},
				returnArgs: []interface{}{nil},
			},
			status: http.StatusCreated,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			userUsecase := new(mocks.UserUsecase)
			userUsecase.On(tt.mockArgs.method, tt.mockArgs.inputArgs...).Return(tt.mockArgs.returnArgs...).Once()

			router := httprouter.New()
			handler := rest.NewUserHandler(userUsecase, validator.New())
			handler.Register(router)

			rr := request("POST", "/users", tt.userCreateReq, router)
			assert.Equalf(t1, tt.status, rr.Code, "return = %v, want %v", rr.Code, tt.status)
		})
	}
}

func TestUserHandler_Get(t *testing.T) {
	type mockArgs struct {
		method     string
		inputArgs  []interface{}
		returnArgs []interface{}
	}
	tests := []struct {
		name     string
		mockArgs mockArgs
		status   int
	}{
		{
			name: "usecase return unexpected error should return internal server error",
			mockArgs: mockArgs{
				method:     "Read",
				inputArgs:  []interface{}{mock.Anything, uint64(1)},
				returnArgs: []interface{}{nil, errors.New("aaaa")},
			},
			status: http.StatusInternalServerError,
		},
		{
			name: "usecase return nil should return success",
			mockArgs: mockArgs{
				method:     "Read",
				inputArgs:  []interface{}{mock.Anything, uint64(1)},
				returnArgs: []interface{}{&model.User{}, nil},
			},
			status: http.StatusOK,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t1 *testing.T) {
			userUsecase := new(mocks.UserUsecase)
			userUsecase.On(tt.mockArgs.method, tt.mockArgs.inputArgs...).Return(tt.mockArgs.returnArgs...).Once()

			router := httprouter.New()
			handler := rest.NewUserHandler(userUsecase, validator.New())
			handler.Register(router)

			rr := request("GET", "/users/1", nil, router)
			assert.Equalf(t1, tt.status, rr.Code, "return = %v, want %v", rr.Code, tt.status)
		})
	}
}
