package rest

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	appErr "gitlab.com/rizaldntr/cards/internal/errors"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/pkg/middleware"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

type TeamHandler struct {
	teamUsecase model.TeamUsecase
	validator   *validator.Validate
}

type TeamCreateRequest struct {
	Name string `json:"name" validate:"required"`
}

type TeamUpdateRequest struct {
	Name   string `json:"name,omitempty"`
	Active bool   `json:"active"`
}

func NewTeamHandler(teamUsecase model.TeamUsecase, validator *validator.Validate) *TeamHandler {
	return &TeamHandler{
		teamUsecase: teamUsecase,
		validator:   validator,
	}
}

func (t *TeamHandler) Register(router *httprouter.Router, middlewares ...middleware.Middleware) {
	router.GET("/teams/:id", middleware.UseMiddleware(t.Get, middlewares...))
	router.POST("/teams", middleware.UseMiddleware(t.Create, middlewares...))
	router.PATCH("/teams/:id", middleware.UseMiddleware(t.Update, middlewares...))
	router.DELETE("/teams/:id", middleware.UseMiddleware(t.Delete, middlewares...))
}

func (t *TeamHandler) Get(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	team, err := t.teamUsecase.Read(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	response.OK(w, team, "Success")

	return nil
}

func (t *TeamHandler) Create(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := TeamCreateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	if err := ValidateRequiredParameters(t.validator, body); len(err) != 0 {
		response.Error(w, err...)

		return errors.WithStack(err[0])
	}

	team := model.Team{
		Name:   body.Name,
		Active: true,
	}

	err := t.teamUsecase.Create(r.Context(), &team)
	if err != nil {
		response.Error(w, err)

		return err
	}

	response.Created(w, team)

	return nil
}

func (t *TeamHandler) Update(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	body := TeamUpdateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}
	defer r.Body.Close()

	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	team := model.Team{
		Name:   body.Name,
		Active: body.Active,
	}

	err = t.teamUsecase.Update(r.Context(), id, &team)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}

func (t *TeamHandler) Delete(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
	id, err := strconv.ParseUint(p.ByName("id"), 10, 64)
	if err != nil {
		response.Error(w, appErr.ErrBadRequest)

		return errors.WithStack(err)
	}

	err = t.teamUsecase.Delete(r.Context(), id)
	if err != nil {
		response.Error(w, err)

		return errors.WithStack(err)
	}

	w.WriteHeader(http.StatusNoContent)

	return nil
}
