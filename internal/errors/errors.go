package errors

import (
	"net/http"

	"github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/rizaldntr/cards/pkg/response"
)

const (
	// ErrorCodeRecordNotFound error code for record not found
	ErrorCodeRecordNotFound = 20000
	// ErrorCodeRecordConflict error code for record conflict
	ErrorCodeRecordConflict = 20001
	// ErrorCodeInvalidParameters error code for invalid parameters
	ErrorCodeInvalidParameters = 20002
	// ErrorCodeNotFound error code for path not found
	ErrorCodeNotFound = 20003
	// ErrorCodeBadRequest error code for bad request
	ErrorCodeBadRequest = 20004
	// ErrorCodeUnprocessableEntity error code for unprocessable entity
	ErrorCodeUnprocessableEntity = 20005
	// ErrorCodeMissingRequiredParameter error code for missing required parameters
	ErrorCodeMissingRequiredParameter = 20006

	// UniqueConstraintError error code for record conflict in Postgres
	UniqueConstraintError = pq.ErrorCode("23505")
)

var (
	// ErrRecordNotFound CustomError for record not found
	ErrRecordNotFound = response.CustomError{
		Code:     ErrorCodeRecordNotFound,
		Message:  "Record not found",
		HTTPCode: http.StatusNotFound,
		Err:      errors.New("record not found"),
	}

	// ErrRecordConflict CustomError for record conflict
	ErrRecordConflict = response.CustomError{
		Code:     ErrorCodeRecordConflict,
		Message:  "Record conflict",
		HTTPCode: http.StatusConflict,
		Err:      errors.New("record conflict"),
	}

	// ErrInvalidParameters CustomError for invalid parameters
	ErrInvalidParameters = response.CustomError{
		Code:     ErrorCodeInvalidParameters,
		Message:  "Invalid parameters",
		HTTPCode: http.StatusUnprocessableEntity,
		Err:      errors.New("invalid parameters"),
	}

	// ErrPathNotFound custom error for path not found
	ErrPathNotFound = response.CustomError{
		Message:  "Path not found",
		Code:     ErrorCodeNotFound,
		HTTPCode: http.StatusNotFound,
		Err:      errors.New("path not found"),
	}

	ErrUnprocessableEntity = response.CustomError{
		Code:     ErrorCodeUnprocessableEntity,
		Message:  "Unprocessable entity",
		HTTPCode: http.StatusUnprocessableEntity,
		Err:      errors.New("unprocessable entity"),
	}

	ErrBadRequest = response.CustomError{
		Code:     ErrorCodeBadRequest,
		Message:  "Bad request",
		HTTPCode: http.StatusBadRequest,
		Err:      errors.New("bad request"),
	}

	// ErrMissingRequiredParameters CustomError for invalid parameters
	ErrMissingRequiredParameters = response.CustomError{
		Code:     ErrorCodeMissingRequiredParameter,
		Message:  "Missing required parameters",
		HTTPCode: http.StatusUnprocessableEntity,
		Err:      errors.New("missing required parameters"),
	}
)
