package application

import (
	"io"

	"github.com/opentracing/opentracing-go"
	"gitlab.com/rizaldntr/cards/internal/config"
	"gitlab.com/rizaldntr/cards/internal/model"
	"gitlab.com/rizaldntr/cards/internal/repository"
	"gitlab.com/rizaldntr/cards/internal/usecase"
	"gitlab.com/rizaldntr/cards/pkg/db"
	"gitlab.com/rizaldntr/cards/pkg/log"
	"gitlab.com/rizaldntr/cards/pkg/tracer"
	"go.uber.org/zap"
)

// Application holds commonly used app wide data, for ease of DI
type Application struct {
	DB       *db.DB
	Cfg      *config.Config
	Logger   *log.Logger
	Usecases *Usecase
	Tracing  *Tracing
}

type repositories struct {
	userRepository       model.UserRepository
	userWalletRepository model.UserWalletRepository
	userCardRepository   model.UserCardRepository
	teamRepository       model.TeamRepository
	teamWalletRepository model.TeamWalletRepository
}

type Usecase struct {
	UserUsecase       model.UserUsecase
	UserWalletUsecase model.UserWalletUsecase
	UserCardUsecase   model.UserCardUsecase
	TeamUsecase       model.TeamUsecase
	TeamWalletUsecase model.TeamWalletUsecase
}

type Tracing struct {
	Tracer opentracing.Tracer
	Closer io.Closer
}

// Get captures env vars, establishes DB connection and keeps/returns
// reference to both
func Get() (*Application, error) {
	cfg, err := config.Get()
	if err != nil {
		return nil, err
	}

	t, closer := tracer.Get(cfg, "cards")
	opentracing.SetGlobalTracer(t)

	db, err := db.Get(cfg.GetDBConnStr())
	if err != nil {
		return nil, err
	}

	logConfig := zap.NewProductionConfig()
	logConfig.DisableStacktrace = true
	logZap, _ := logConfig.Build(zap.AddCallerSkip(1))
	logger := log.NewCustomLogger(logZap)

	repositories := createRepository(db, logger)
	usecases := createUsecase(repositories, logger)

	return &Application{
		DB:       db,
		Cfg:      cfg,
		Logger:   logger,
		Usecases: usecases,
		Tracing: &Tracing{
			Tracer: t,
			Closer: closer,
		},
	}, nil
}

func createRepository(db *db.DB, logger *log.Logger) *repositories {
	userRepository := repository.NewUserRepository(db, logger)
	userWalletRepository := repository.NewUserWalletRepository(db, logger)
	userCardRepository := repository.NewUserCardRepository(db, logger)
	teamRepository := repository.NewTeamRepository(db, logger)
	teamWalletRepository := repository.NewTeamWalletRepository(db, logger)

	return &repositories{
		userRepository:       userRepository,
		userWalletRepository: userWalletRepository,
		userCardRepository:   userCardRepository,
		teamRepository:       teamRepository,
		teamWalletRepository: teamWalletRepository,
	}
}

func createUsecase(repositories *repositories, logger *log.Logger) *Usecase {
	userUsecase := usecase.NewUserUsecase(repositories.userRepository, logger)
	userWalletUsecase := usecase.NewUserWalletUsecase(repositories.userWalletRepository, logger)
	userCardUsecase := usecase.NewUserCardUsecase(repositories.userCardRepository, logger)
	teamUsecase := usecase.NewTeamUsecase(repositories.teamRepository, logger)
	teamWalletUsecase := usecase.NewTeamWalletUsecase(repositories.teamWalletRepository, logger)

	return &Usecase{
		UserUsecase:       userUsecase,
		UserWalletUsecase: userWalletUsecase,
		UserCardUsecase:   userCardUsecase,
		TeamUsecase:       teamUsecase,
		TeamWalletUsecase: teamWalletUsecase,
	}
}
