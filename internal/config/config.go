package config

import (
	"fmt"

	"github.com/joeshaw/envdecode"
	"github.com/subosito/gotenv"
)

type Config struct {
	Application  ApplicationConfig
	Postgres     PostgresConfig
	JeagerConfig JeagerConfig
}

type PostgresConfig struct {
	DBUsername string `env:"DATABASE_USERNAME, required"`
	DBPassword string `env:"DATABASE_PASSWORD, required"`
	DBHost     string `env:"DATABASE_HOST, required"`
	DBPort     string `env:"DATABASE_PORT, required"`
	DBName     string `env:"DATABASE_NAME, required"`
}

type ApplicationConfig struct {
	Port string `env:"PORT, required"`
}

type JeagerConfig struct {
	SamplingServerURL string  `env:"JAEGER_SAMPLING_SERVER_URL"`
	LocalAgentAddress string  `env:"JAEGER_LOCAL_AGENT_ADDRESS"`
	SamplingType      string  `env:"JAEGER_SAMPLING_TYPE"`
	SamplingValue     float64 `env:"JAEGER_SAMPLING_VALUE"`
}

func Get() (*Config, error) {
	var cfg Config
	err := gotenv.Load(".env")
	if err != nil {
		return nil, err
	}

	err = envdecode.Decode(&cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}

func (c *Config) GetDBConnStr() string {
	return c.getDBConnStr(c.Postgres.DBHost, c.Postgres.DBName)
}

func (c *Config) getDBConnStr(dbHost, dbname string) string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		c.Postgres.DBUsername,
		c.Postgres.DBPassword,
		dbHost,
		c.Postgres.DBPort,
		dbname,
	)
}

func (c *Config) GetAPIPort() string {
	return ":" + c.Application.Port
}
