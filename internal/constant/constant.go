package constant

type ContextKey string

const (
	LogFieldNameGlobal = "cards"
	LogFieldNameError  = "error"
	LogFieldNameInfo   = "info"

	ContextKeyIPAddress ContextKey = "ip_address"
	ContextKeyRequestID ContextKey = "request_id"

	DefaultDailyLimit   = 15000
	DefaultMonthlyLimit = 200000
)
