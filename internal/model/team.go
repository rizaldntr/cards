package model

import (
	"context"
	"time"
)

type Team struct {
	ID        uint64    `json:"id"`
	Name      string    `json:"name,omitempty" gorm:"default:null"`
	Active    bool      `json:"active" gorm:"default:null"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	Users     []User    `gorm:"many2many:user_teams;" json:"users,omitempty"`
}

type TeamUsecase interface {
	Create(ctx context.Context, team *Team) error
	Update(ctx context.Context, id uint64, team *Team) error
	Read(ctx context.Context, id uint64) (*Team, error)
	Delete(ctx context.Context, id uint64) error
}

type TeamRepository interface {
	Create(ctx context.Context, team *Team) error
	Update(ctx context.Context, id uint64, team *Team) error
	Read(ctx context.Context, id uint64) (*Team, error)
	Delete(ctx context.Context, id uint64) error
}
