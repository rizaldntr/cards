package model

import (
	"context"
	"time"

	"github.com/shopspring/decimal"
)

type UserCard struct {
	ID           uint64          `gorm:"primaryKey" json:"id"`
	UserWalletID uint64          `gorm:"default:null" json:"userWalletId"`
	Name         string          `gorm:"default:null" json:"name,omitempty"`
	Number       string          `gorm:"default:null" json:"number"`
	Valid        time.Time       `gorm:"default:null" json:"valid"`
	Active       bool            `gorm:"default:null" json:"active"`
	DailyLimit   decimal.Decimal `gorm:"default:null" json:"dailyLimit"`
	DailyUsage   decimal.Decimal `gorm:"default:null" json:"dailyUsage"`
	MonthlyLimit decimal.Decimal `gorm:"default:null" json:"monthlyLimit"`
	MonthlyUsage decimal.Decimal `gorm:"default:null" json:"monthlyUsage"`
	CreatedAt    time.Time       `json:"createdAt"`
	UpdatedAt    time.Time       `json:"updatedAt"`
}

type UserCardUsecase interface {
	Create(ctx context.Context, team *UserCard) error
	Update(ctx context.Context, id uint64, team *UserCard) error
	Read(ctx context.Context, id uint64) (*UserCard, error)
	Delete(ctx context.Context, id uint64) error
	Transactions(ctx context.Context, from, to uint64, amount decimal.Decimal) error
}

type UserCardRepository interface {
	Create(ctx context.Context, team *UserCard) error
	Update(ctx context.Context, id uint64, team *UserCard) error
	Read(ctx context.Context, id uint64) (*UserCard, error)
	Delete(ctx context.Context, id uint64) error
	Transactions(ctx context.Context, from, to uint64, amount decimal.Decimal) error
}
