package model

import (
	"context"
	"time"
)

type User struct {
	ID        uint64       `gorm:"primaryKey" json:"id"`
	CreatedAt time.Time    `json:"createdAt"`
	UpdatedAt time.Time    `json:"updatedAt"`
	Name      string       `gorm:"default:null" json:"name,omitempty"`
	Username  string       `gorm:"default:null" json:"username,omitempty"`
	Password  string       `gorm:"default:null" json:"-"`
	Phone     string       `gorm:"default:null" json:"phone"`
	Email     string       `gorm:"default:null" json:"email,omitempty"`
	Active    bool         `gorm:"default:null" json:"active"`
	Wallets   []UserWallet `json:"wallets,omitempty"`
	Teams     []Team       `gorm:"many2many:user_teams;" json:"teams,omitempty"`
}

type UserUsecase interface {
	Create(ctx context.Context, team *User) error
	Update(ctx context.Context, id uint64, team *User) error
	Read(ctx context.Context, id uint64) (*User, error)
	Delete(ctx context.Context, id uint64) error
}

type UserRepository interface {
	Create(ctx context.Context, team *User) error
	Update(ctx context.Context, id uint64, team *User) error
	Read(ctx context.Context, id uint64) (*User, error)
	Delete(ctx context.Context, id uint64) error
}
