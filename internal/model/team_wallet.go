package model

import (
	"context"
	"database/sql"
	"time"

	"github.com/shopspring/decimal"
)

type TeamWallet struct {
	ID           uint64          `gorm:"primaryKey" json:"id"`
	TeamID       uint64          `json:"teamID" gorm:"default:null"`
	Name         string          `json:"name,omitempty" gorm:"default:null"`
	Balance      decimal.Decimal `json:"balance" gorm:"default:null"`
	Active       sql.NullBool    `json:"active" gorm:"default:null"`
	DailyLimit   decimal.Decimal `json:"dailyLimit" gorm:"default:null"`
	DailyUsage   decimal.Decimal `json:"dailyUsage" gorm:"default:null"`
	MonthlyLimit decimal.Decimal `json:"monthlyLimit" gorm:"default:null"`
	MonthlyUsage decimal.Decimal `json:"monthlyUsage" gorm:"default:null"`
	CreatedAt    time.Time       `json:"createdAt"`
	UpdatedAt    time.Time       `json:"updatedAt"`
}

type TeamWalletUsecase interface {
	Create(ctx context.Context, team *TeamWallet) error
	Update(ctx context.Context, id uint64, team *TeamWallet) error
	Read(ctx context.Context, id uint64) (*TeamWallet, error)
	Delete(ctx context.Context, id uint64) error
}

type TeamWalletRepository interface {
	Create(ctx context.Context, team *TeamWallet) error
	Update(ctx context.Context, id uint64, team *TeamWallet) error
	Read(ctx context.Context, id uint64) (*TeamWallet, error)
	Delete(ctx context.Context, id uint64) error
}
