package model

import (
	"context"
	"time"

	"github.com/shopspring/decimal"
)

type UserWallet struct {
	ID        uint64          `gorm:"primaryKey" json:"id"`
	UserID    uint64          `gorm:"default:null" json:"userId"`
	Name      string          `gorm:"default:null" json:"name,omitempty"`
	Balance   decimal.Decimal `gorm:"default:null" json:"balance"`
	Active    bool            `gorm:"default:null" json:"active"`
	CreatedAt time.Time       `json:"createdAt"`
	UpdatedAt time.Time       `json:"updatedAt"`
}

type UserWalletUsecase interface {
	Create(ctx context.Context, team *UserWallet) error
	Update(ctx context.Context, id uint64, team *UserWallet) error
	Read(ctx context.Context, id uint64) (*UserWallet, error)
	Delete(ctx context.Context, id uint64) error
	Topup(ctx context.Context, id uint64, amount decimal.Decimal) error
}

type UserWalletRepository interface {
	Create(ctx context.Context, team *UserWallet) error
	Update(ctx context.Context, id uint64, team *UserWallet) error
	Read(ctx context.Context, id uint64) (*UserWallet, error)
	Delete(ctx context.Context, id uint64) error
	Topup(ctx context.Context, id uint64, amount decimal.Decimal) error
}
