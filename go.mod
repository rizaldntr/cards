module gitlab.com/rizaldntr/cards

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/daixiang0/gci v0.2.9 // indirect
	github.com/go-playground/validator/v10 v10.10.0
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0
	github.com/jackc/pgx/v4 v4.14.1 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/joeshaw/envdecode v0.0.0-20200121155833-099f1fc765bd
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.10.4
	github.com/mgechev/revive v1.1.2 // indirect
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.11.0
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	github.com/shopspring/decimal v1.2.0
	github.com/stretchr/testify v1.7.0
	github.com/subosito/gotenv v1.2.0
	github.com/uber/jaeger-client-go v2.30.0+incompatible // indirect
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	go.uber.org/zap v1.19.1
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	golang.org/x/sys v0.0.0-20211213223007-03aa0b5f6827 // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.4
	gorm.io/plugin/opentracing v0.0.0-20211220013347-7d2b2af23560 // indirect
	k8s.io/klog v1.0.0 // indirect
	mvdan.cc/gofumpt v0.2.1 // indirect
)
