CREATE TABLE IF NOT EXISTS team_wallets (
    id SERIAL PRIMARY KEY,
    team_id SERIAL NOT NULL REFERENCES teams (id),
    name VARCHAR(50) NOT NULL,
    balance DECIMAL(15, 2) NOT NULL CHECK (balance > 0.00),
    active BOOLEAN DEFAULT TRUE,
    daily_limit DECIMAL(15, 2) NOT NULL CHECK (daily_limit >= 0.00),
    daily_usage DECIMAL(15, 2) NOT NULL CHECK (daily_usage >= 0.00 AND daily_usage <= daily_limit),
    monthly_limit DECIMAL(15, 2) NOT NULL CHECK (monthly_limit >= 0.00),
    monthly_usage DECIMAL(15, 2) NOT NULL CHECK (monthly_limit >= 0.00 AND monthly_limit <= monthly_usage),
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);
