CREATE TABLE IF NOT EXISTS user_teams (
    id SERIAL PRIMARY KEY,
    user_id SERIAL NOT NULL REFERENCES users (id),
    team_id SERIAL NOT NULL REFERENCES teams (id),
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
)
