CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    username VARCHAR ( 50 ) UNIQUE NOT NULL,
    password VARCHAR ( 50 ) NOT NULL,
    name VARCHAR ( 255 ) NOT NULL,
    phone VARCHAR (20) NOT NULL UNIQUE,
    email VARCHAR ( 255 ) UNIQUE NOT NULL,
    active boolean NOT NULL DEFAULT TRUE,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);
