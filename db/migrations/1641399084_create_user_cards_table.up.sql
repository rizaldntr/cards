CREATE TABLE IF NOT EXISTS user_cards (
    id SERIAL PRIMARY KEY,
    user_wallet_id SERIAL NOT NULL REFERENCES user_wallets (id),
    name VARCHAR(50) NOT NULL,
    number VARCHAR (50) NOT NULL UNIQUE,
    valid DATE NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    daily_limit DECIMAL(15, 2) NOT NULL CHECK(daily_limit >= 0.00),
    daily_usage DECIMAL(15, 2) NOT NULL CHECK(daily_usage >= 0.00 AND daily_usage <= daily_limit),
    monthly_limit DECIMAL(15, 2) NOT NULL CHECK(monthly_limit >= 0.00),
    monthly_usage DECIMAL(15, 2) NOT NULL CHECK(monthly_usage >= 0.00 AND monthly_usage <= monthly_limit),
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);
