module.exports = {
    html: {
        playground: {
            enabled: true,
            env: "development",
            environments: {
                development: {
                    url: "http://127.0.0.1:8000/",
                    playground: true
                },
            }
        }
    }
};