## Cards [/cards]

### Create New Card [POST /cards]

Endpoint to create new card.

+ Request (application/json)

  Request with valid data will return success

    + Attributes (object)
        + Include Card-Create-Request

+ Response 200 (application/json)
    + Attributes (object)
        + data (object, required)
            + Include Card
        + meta (object, required)
            + Include HTTP-Created

+ Request (application/json)

  Request without data will return bad request
    + Attributes (object, nullable)

+ Response 400 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Bad-Request
        + meta (object, required)
            + Include HTTP-Bad-Request

+ Request (application/json)

  Request without required parameters will return bad request

    + Attributes (object)
        + name: `Blue Card` (string, required)

+ Response 422 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + message: `Missing required parameters` (string, required)
                + code: 20002 (number, required)
                + field: `userwalletid`
        + meta (object, required)
            + Include HTTP-Unprocessable-Entity

### Get Card [GET /cards/{id}]

Endpoint to get card's data. If card found return success with card's data.

+ Parameters
    + id: 1 (number, required) - Card ID of selected card

+ Response 200 (application/json)
    + Attributes (object)
        + data (object, required)
            + Include Card
        + meta (object, required)
            + Include HTTP-Created

+ Parameters
    + id: 2 (number, required) - Card ID of selected card

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found

### Update Card [PATCH /cards/{id}]

Endpoint to update card's data.

+ Request (application/json)

  Request with valid data will return success

    + Attributes (object)
        + Include Card-Update-Request

+ Response 204 (application/json)

+ Request (application/json)

    + Attributes (object, nullable)

+ Response 400 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Bad-Request
        + meta (object, required)
            + Include HTTP-Bad-Request

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found
            

### Delete Card [DELETE /cards/{id}]

Endpoint to delete card, if card found return 204 otherwise return 404

+ Parameters
    + id: 1 (number, required) - Card ID of selected card

+ Response 204 (application/json)

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found

### Create Card Transactions [POST /card-transactions]

Endpoint to delete card, if card found return 204 otherwise return 422

+ Request (application/json)

    + Attributes
      + from: 1 (number, required)
      + to: 2 (number, required)
      + amount: 200.00 (number, required)

+ Response 204 (application/json)
