## Wallets [/wallets]

### Create New Wallet [POST /wallets]

Endpoint to create new wallet.

+ Request (application/json)

  Request with valid data will return success

    + Attributes (object)
        + Include Wallet-Create-Request

+ Response 200 (application/json)
    + Attributes (object)
        + data (object, required)
            + Include Wallet
        + meta (object, required)
            + Include HTTP-Created

+ Request (application/json)

  Request without data will return bad request
    + Attributes (object, nullable)

+ Response 400 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Bad-Request
        + meta (object, required)
            + Include HTTP-Bad-Request

+ Request (application/json)

  Request without required parameters will return bad request

    + Attributes (object)
        + name: `John Doe` (string, required)

+ Response 422 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + message: `Missing required parameters` (string, required)
                + code: 20002 (number, required)
                + field: `userid`
        + meta (object, required)
            + Include HTTP-Unprocessable-Entity
            

### Get Wallet [GET /wallets/{id}]

Endpoint to get wallet's data. If wallet found return success with wallet's data.

+ Parameters
    + id: 1 (number, required) - Wallet ID of selected wallet

+ Response 200 (application/json)
    + Attributes (object)
        + data (object, required)
            + Include Wallet
        + meta (object, required)
            + Include HTTP-Created

+ Parameters
    + id: 2 (number, required) - Wallet ID of selected wallet

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found

### Update Wallet [PATCH /wallets/{id}]

Endpoint to update wallet's data.

+ Request (application/json)

  Request with valid data will return success

    + Attributes (object)
        + Include Wallet-Update-Request

+ Response 204 (application/json)

+ Request (application/json)

    + Attributes (object)
        + Include Wallet-Update-Request

+ Response 400 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Bad-Request
        + meta (object, required)
            + Include HTTP-Bad-Request

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found

### Delete Wallet [DELETE /wallets/{id}]

Endpoint to delete wallet, if wallet found return 204 otherwise return 404

+ Parameters
    + id: 1 (number, required) - Wallet ID of selected wallet

+ Response 204 (application/json)

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found

### Topup Wallet [POST /topup-wallets]

Endpoint to delete card, if card found return 204 otherwise return 422

+ Request (application/json)
    + Attributes
      + id: 1 (number, required)
      + amount: 200.00 (number, required)

+ Response 204 (application/json)
