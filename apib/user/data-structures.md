# Data Structures

## `Common-Timestamp` (object)

+ createdAt: `2015-11-01T12:34:05.000Z` (string, required)
+ updatedAt: `2015-11-01T12:34:05.000Z` (string, required)

## `User-Create-Request` (object)

+ name: `John Doe` (string, required)
+ username: `johndoe`
+ email: `johndoe@gmail.com` (string, required)
+ phone: `6281234567890` (string, required)
+ password: `veryStrongPasswordRight?`

## `User-Update-Request` (object)

+ name: `John Doe` (string, required)
+ username: `johndoe`
+ email: `johndoe@gmail.com` (string, required)
+ phone: `6281234567890` (string, required)
+ active: true (boolean, required)

## `User` (object)

+ id: 1 (number, required)
+ Include User-Update-Request
+ Include Common-Timestamp

## `Wallet-Create-Request` (object)

+ name: `My Personal Wallet` (string, required)
+ userId: 1 (number, required)

## `Wallet-Update-Request` (object)

+ name: `My Personal Wallet` (string, required)
+ active: true (boolean, required)

## `Wallet` (object)

+ id: 1 (number, required)
+ userId: 1 (number, required)
+ balance: 1000.23 (number, required)
+ Include Wallet-Update-Request
+ Include Common-Timestamp

## `Card-Create-Request` (object)

+ name: `Blue Card` (string, required)
+ userWalletId: 1 (number, required)

## `Card-Update-Request` (object)

+ name: `Blue Card` (string, required)
+ active: true (boolean, required)
+ dailyLimit: 150.00 (number, required)
+ monthlyLimit: 150000.00 (number, required)

## `Card` (object)

+ id: 1 (number, required)
+ userWalletId: 1 (number, required)
+ balance: 1000.23 (number, required)
+ dailyUsage: 1.00 (number, required)
+ monthlyUsage: 1400.00 (number, required)
+ Include Card-Update-Request
+ Include Common-Timestamp