## Users [/users]

### Create New User [POST /users]

Endpoint to create new user.

+ Request (application/json)

  Request with valid data will return success

    + Attributes (object)
        + Include User-Create-Request

+ Response 200 (application/json)
    + Attributes (object)
        + data (object, required)
          + Include User
        + meta (object, required)
          + Include HTTP-Created

+ Request (application/json)

  Request without data will return bad request
    + Attributes (object, nullable)

+ Response 400 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
              + Include Error-Bad-Request
        + meta (object, required)
            + Include HTTP-Bad-Request

+ Request (application/json)

  Request without required parameters will return bad request

    + Attributes (object)
      + name: `John Doe` (string, required)
      + email: `rizal@rizal.com` (string, required)
      + phone: `08651234567` (string, required)

+ Response 422 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + message: `Missing required parameters` (string, required)
                + code: 20002 (number, required)
                + field: `password`
            + (object)
                + message: `Missing required parameters` (string, required)
                + code: 20002 (number, required)
                + field: `username`
        + meta (object, required)
            + Include HTTP-Unprocessable-Entity

+ Request (application/json)

  Request with invalid data, using non-unique data will return conflict status

    + Attributes (object)
        + Include User-Create-Request

+ Response 409 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + message: `Record conflict` (string, required)
                + code: 20001 (number, required)
        + meta (object, required)
            + Include HTTP-Conflict


### Get User [GET /users/{id}]

Endpoint to get user's data. If user found return success with user's data.

+ Parameters
    + id: 1 (number, required) - User ID of selected user

+ Response 200 (application/json)
    + Attributes (object)
        + data (object, required)
            + Include User
        + meta (object, required)
            + Include HTTP-Created

+ Parameters
    + id: 2 (number, required) - User ID of selected user

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
          + (object)
              + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found

### Update User [PATCH /users/{id}]

Endpoint to update user's data.

+ Request (application/json)

  Request with valid data will return success

    + Attributes (object)
        + Include User-Create-Request

+ Response 204 (application/json)

+ Request (application/json)

    + Attributes (object)
        + Include User-Update-Request

+ Response 400 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Bad-Request
        + meta (object, required)
            + Include HTTP-Bad-Request

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found

+ Request (application/json)

  Request with invalid data, using non-unique data will return conflict status

    + Attributes (object)
        + Include User-Update-Request

+ Response 409 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + message: `Record conflict` (string, required)
                + code: 20001 (number, required)
        + meta (object, required)
            + Include HTTP-Conflict

### Delete User [DELETE /users/{id}]

Endpoint to delete user, if user found return 204 otherwise return 404

+ Parameters
    + id: 1 (number, required) - User ID of selected user

+ Response 204 (application/json)

+ Response 404 (application/json)
    + Attributes (object)
        + errors (array, required)
            + (object)
                + Include Error-Record-Not-Found
        + meta (object, required)
            + Include HTTP-Not-Found