# Data Structures

## `HTTP-OK` (object)

+ http_status: 200 (number, required)

## `HTTP-Created` (object)

+ http_status: 201 (number, required)

## `HTTP-Accepted` (object)

+ http_status: 202 (number, required)

## `HTTP-No-Content` (object)

+ http_status: 204 (number, required)

## `HTTP-Bad-Request` (object)

+ http_status: 400 (number, required)

## `HTTP-Unauthorized` (object)

+ http_status: 401 (number, required)

## `HTTP-Payment-Required` (object)

+ http_status: 402 (number, required)

## `HTTP-Forbidden` (object)

+ http_status: 403 (number, required)

## `HTTP-Not-Found` (object)

+ http_status: 404 (number, required)

## `HTTP-Conflict` (object)

+ http_status: 409 (number, required)

## `HTTP-Unprocessable-Entity` (object)

+ http_status: 422 (number, required)

## `Error-Bad-Request` (object)

+ message: `Bad request` (string, required)
+ code: 20004 (number, required)

## `Error-Record-Not-Found` (object)

+ message: `Record not found` (string, required)
+ code: 20000 (number, required)
