FORMAT: 1A
HOST: http://localhost:8000

# Spenmo API

This is a *draft* version of Spenmo API for managing users, wallets, cards, teams resources.

{{partial "./user/api.md"}}
{{partial "./team/api.md"}}
{{partial "data-structures.md"}}
