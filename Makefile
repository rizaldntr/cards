MIGRATIONS_PATH = db/migrations
LOGFILE = $(MIGRATIONS_PATH)/$(shell date +%s)

dep-lint:
	@go get github.com/mgechev/revive@latest
	@curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.43.0

dep-mockery:
	@go get github.com/vektra/mockery/v2/.../

dep-migrate:
	@go get -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

dep-pretty:
	@go get mvdan.cc/gofumpt@latest
	@go get github.com/daixiang0/gci

dep-all: dep-lint dep-mockery dep-migrate

test: lint
	go test -race -cover -coverprofile=coverage.out $$(go list ./... | grep -Ev "cmd|application|constant|db|config|mock|model")

lint: dep-lint pretty
	./bin/golangci-lint run -v

coverage: test
	go tool cover -html=coverage.out

pretty: dep-pretty
	gofumpt -d -w $$(find . -type f -name '*.go')
	gci -d -w $$(find . -type f -name '*.go')
	gofmt -d -w $$(find . -type f -name '*.go')
	goimports -d -w $$(find . -type f -name '*.go')
	go vet ./...

mockery: dep-mockery
	mockery --dir $(dir) --name $(name)

generate-migration:
	touch "$(LOGFILE)_${title}.up.sql"
	touch "$(LOGFILE)_${title}.down.sql"

magic:
	docker-compose up --build