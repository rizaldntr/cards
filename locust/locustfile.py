import random, string
from locust import HttpUser, task, between

# {
#     "name": "John Doe",
#     "username": "johndoe",
#     "email": "johndoe@gmail.com",
#     "phone": "6281234567890",
#     "password": "veryStrongPasswordRight?"
# }
chars = string.ascii_lowercase+string.digits

class User(HttpUser):
    wait_time = between(1, 5)

    @task(50)
    def create_user(self):
        prefix = ''.join(random.sample(chars,10))
        user = {
            "name": "John Doe {}".format(prefix),
            "username": "johndoe_{}".format(prefix),
            "email": "johndoe.{}".format(prefix),
            "phone": ''.join([str(random.randint(0, 9)) for i in range(10)]),
            "password": "password-{}".format(prefix)
        }

        response = self.client.post("/users", json=user)
        if response.ok:
            self.max_user_id = response.json()['data']['id']

    @task(100)
    def get_user(self):
        if self.max_user_id:
            self.client.get('/users/{}'.format(random.randint(0, self.max_user_id)))

    @task(30)
    def update_user(self):
        if self.max_user_id:
            prefix = ''.join(random.sample(chars,10))
            user = {
                "name": "John Doe {}".format(prefix),
                "username": "johndoe_{}".format(prefix),
                "email": "johndoe.{}".format(prefix),
                "phone": ''.join([str(random.randint(0, 9)) for i in range(10)]),
                "password": "password-{}".format(prefix),
                "active": (random.randint(1,2) == 1)
            }
            self.client.patch("/users/{}".format(random.randint(0, self.max_user_id)), json=user)

    @task(10)
    def delete_user(self):
        if self.max_user_id:
            self.client.delete('/users/{}'.format(random.randint(0, self.max_user_id)))
